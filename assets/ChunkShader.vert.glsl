#version 450 core

layout(location = 0) in float aCurrentBlockPosition;
layout(location = 1) in float aData;
layout(location = 2) in vec3 aPosition;

struct DataOut{
   vec3 FragPos;
   vec3 Normal;
   vec3 viewPos;
   vec3 lightPos;
   vec3 TexCoords;
};

layout(location = 0) flat out vec3 data;
layout(location = 1) out DataOut Output;

layout(location = 0) uniform mat4 u_Projection;
layout(location = 1) uniform mat4 u_View;
layout(location = 2) uniform mat4 u_Model;

layout(location = 3) uniform vec4 u_DiffuseColor;
layout(location = 4) uniform vec3 u_ViewPos;
layout(location = 5) uniform vec3 u_LightPos;

vec2 texCoords[4] = 
{
	vec2(1,1),
	vec2(1,0),
	vec2(0,0),
	vec2(0,1)
};
int TextureAtals[6][6] = {
{0,0,0,0,0,0},//air
{0,0,0,0,0,0},//dirt
{1,1,2,0,1,1},//grass,
{3,3,3,3,3,3},//stone
{4,4,4,4,4,4},//sand
{5,5,5,5,5,5},//water
};


void main()
{

	uint index = (uint(aData)&0xff);
	uint blockID = ((uint(aData) & 0xff0000)>>16);	
	uint faceID = ((uint(aData) & 0xff00)>>8);
	
	uint z = (uint(aPosition)&0xff);
	uint y = ((uint(aPosition) & 0xff00)>>8);
	uint x = ((uint(aPosition) & 0xff0000)>>16);	
	
	int location = TextureAtals[blockID][faceID];
	float unitSize = 0.125;
	
	vec3 Normals[6] = {
		vec3(0,0,1),
		vec3(0,0,-1),
		vec3(0,1,0),
		vec3(0,-1,0),
		vec3(-1,0,0),
		vec3(1,0,0)
	};
	vec3 Normal = Normals[faceID];
	
	data = vec3(index,faceID,blockID);
	Output.TexCoords = vec3(texCoords[index],location);
    Output.FragPos = vec3(u_Model*vec4(aPosition,1.0));
    Output.Normal = mat3(transpose(inverse(u_Model))) * Normal;
    Output.viewPos = u_ViewPos;
    Output.lightPos = u_LightPos;

    vec4 position = u_Projection * u_View * u_Model* vec4(aPosition,1.0);
    gl_Position = position;
}