#version 450

layout(location = 0) out vec4 FragColor;
struct DataOut{
   vec3 FragPos;
   vec3 Normal;
   vec3 viewPos;
   vec3 lightPos;
   vec3 TexCoords;
};

vec3 colors[4] = 
{
	vec3(1,0,0),
	vec3(0,0,1),
	vec3(0,1,0),
	vec3(1,1,1),
};

layout(location = 0) in vec3 data;
layout(location = 1) in DataOut DataInput;
layout(binding=0) uniform sampler2DArray ourTexture;
void main()
{
	vec3 lightColor = vec3(1,1,1);
	float ambientStrength = 0.2;
    vec3 ambient = ambientStrength * lightColor;

// diffuse 
    vec3 norm = normalize(DataInput.Normal);
    vec3 lightDir = normalize(DataInput.lightPos - DataInput.FragPos);
    float diff = max(dot(norm, lightDir), 1.0);
    vec3 diffuse = diff * lightColor;
    
    // specular
    float specularStrength = 1.0;
    vec3 viewDir = normalize(DataInput.viewPos - DataInput.FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 256);
    vec3 specular = specularStrength * spec * lightColor;  
    
	vec3 sideColorMultiplier = vec3(1,1,1);
	
	float face = data.y;
	
	if(face == 0 || face == 1){
		sideColorMultiplier = vec3(0.86,0.86,0.86);
	}else
	if(face == 4 || face == 5){
		sideColorMultiplier = vec3(0.8,0.8,0.8);
	}
	vec3 textureColor = texture(ourTexture,DataInput.TexCoords).rgb;
	
	vec3 result = (ambient + diffuse) *sideColorMultiplier* textureColor;

    FragColor = vec4(result,1.0);

	//FragColor = DataInput.Color;
}