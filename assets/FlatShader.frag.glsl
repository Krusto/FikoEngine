#version 450 core

layout(location = 0) out vec4 FragColor;
struct DataOut{
   vec4 Color;
};

layout(location = 0) in DataOut DataInput;
void main()
{
	FragColor = DataInput.Color;
}