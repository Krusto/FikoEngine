#version 450 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoord;

struct DataOut{
   vec2 TexCoords;
};

layout(location = 0) out DataOut Output;

layout(location = 0) uniform mat4 u_Projection;
layout(location = 1) uniform mat4 u_View;
layout(location = 2) uniform mat4 u_Model;

void main()
{

	Output.TexCoords = aTexCoord;

    vec4 position = u_Projection * u_View * u_Model* vec4(aPosition,1.0);
    gl_Position = position;
}