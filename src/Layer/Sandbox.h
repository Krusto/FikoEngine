#pragma once
#include <glad/glad.h>
#include <imgui.h>
#include <Layer/Layer.h>
#include <Core/Window.h>
#include <Renderer/Renderer.h>
#include <memory>
#include <thread>
#include <chrono>
class Sandbox : public Layer{
public:
    Sandbox(){
        m_Name = "Sandbox";
    }
    void Init(Window* window) override {
        m_Window = window;
        width = window->width;
        height = window->height;


        framebuffer = Framebuffer::Create(width, height);
        camera = Camera(CameraSpec(ViewportSize{ width,height }, 45.0f, 0.1f, 100.0f));
        camera.Move({ 0.0f,0.0f,3.0f });
        basicShader = Shader::Load("./assets/vertex.glsl","./assets/fragment.glsl");
        flatShader = Shader::Load("./assets/FlatShaderVert.glsl","./assets/FlatShaderFrag.glsl");

        //texture0 = Texture::Create("./assets/textures/metal-splotchy-albedo.png");
        //std::cout << "loading texture 1 complete!\n";
        //texture1 = Texture::Create("./assets/textures/metal-splotchy-normal-ogl.png");
        //std::cout << "loading texture 2 complete!\n";
        //texture2 = Texture::Create("./assets/textures/metal-splotchy-metal.png");
        //std::cout << "loading texture 3 complete!\n";
        //texture3 = Texture::Create("./assets/textures/metal-splotchy-rough.png");
        //std::cout << "loading texture 4 complete!\n";
        //texture4 = Texture::Create("./assets/textures/slipperystonework-ao.png");
        //std::cout << "loading texture 5 complete!\n";

        light = Light::Create({ 0.0f,0.0f,10.0f }, { 150.0f,150.0f,150.0f }, 1, { 0.0f,0.0f,0.0f }, LightType::Point);

        object = Object3D::Create("./assets/sphere.obj", { 0.0f,0.0f,0.0f }, { 0.0f,0.0f,0.0f });
        std::cout << "loading obj complete!\n";
    };
    void OnAttach() override {}
    void OnUpdate(float dt) override{
        framebuffer->Bind();
        Renderer::ClearColor({ 0.2,0.3,0.4,1.0 });
       /* basicShader->Use();
        basicShader->setInt("albedoMap", 0);
        basicShader->setInt("normalMap", 1);
        basicShader->setInt("metallicMap", 2);
        basicShader->setInt("roughnessMap", 3);
        basicShader->setInt("aoMap", 4);


        basicShader->setMat4("projection", camera.getProjection());
        basicShader->setMat4("view", camera.getView());
        basicShader->setVec3("camPos", camera.GetPosition());

        texture0->Bind(0);
        texture1->Bind(1);
        texture2->Bind(2);
        texture3->Bind(3);
        texture4->Bind(4);

        float nrRows = 7, nrColumns = 7, spacing = 2.5;
        glm::mat4 model = glm::mat4(1.0f);
        for (int row = 0; row < nrRows; ++row)
        {
            for (int col = 0; col < nrColumns; ++col)
            {
                model = glm::mat4(1.0f);
                model = glm::translate(model, glm::vec3(
                    (float)(col - (nrColumns / 2)) * spacing,
                    (float)(row - (nrRows / 2)) * spacing,
                    0.0f
                ));
                model = glm::rotate(model, sinf(glfwGetTime()), glm::vec3(0.0, 1.0, 0.0));
                basicShader->setMat4("model", model);

                object.Bind();
                Renderer::DrawIndexed(object.VA());
            }
        }

        glm::vec3 newPos = light.GetPosition() + glm::vec3(sin(glfwGetTime() * 5.0) * 5.0, 0.0, 0.0);
        newPos = light.GetPosition();
        basicShader->setVec3("lightPositions[0]", newPos);
        basicShader->setVec3("lightColors[0]", light.GetColor());

        model = glm::mat4(1.0f);
        model = glm::translate(model, newPos);
        model = glm::scale(model, glm::vec3(0.5f));
        basicShader->setMat4("model", model);
        object.Bind();
        Renderer::DrawIndexed(object.VA());
        */
        flatShader->Use();
        Renderer::DrawQuad2D({ 0.0,0.0 }, { 1.0,1.0 }, { 0.4,1.0,0.6,0.5});
        framebuffer->Unbind();

        ImGui::Begin("Viewport");
        ImGui::Image((void*)framebuffer->GetColorAttachmentID(), ImVec2(900,600.0));
        ImGui::End();

        m_DT = dt;
    }
    void OnWindowResizeEvent(int width,int height) override{
        framebuffer->Resize(width, height);
    }
    void OnMouseMoveEvent(int x,int y) override{
        
        if (glfwGetMouseButton(m_Window->GetHandle(), GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
            if (firstMouse)
            {
                lastX = x;
                lastY = y;
                firstMouse = false;
            }

            float xoffset = x - lastX;
            float yoffset = lastY - y; // reversed since y-coordinates go from bottom to top

            lastX = x;
            lastY = y;

            camera.ProcessMouseMovement(xoffset, yoffset,m_DT);
        }
        else if(glfwGetMouseButton(m_Window->GetHandle(),GLFW_MOUSE_BUTTON_LEFT)==GLFW_RELEASE){
            firstMouse = true;
        }
    };
    void OnKeyboardEvent(int action,int key) override{
        if (action == GLFW_PRESS && key == GLFW_KEY_ESCAPE) {
            this->SetShouldExit(true);
        }
        camera.ProcessKeyboardInput(action, key, m_DT);
    };
    void OnWindowShouldCloseEvent() override {
        this->SetShouldExit(true);
    }
    void OnDetach() override{};
    void Destroy() override{
    };
private:
    bool firstMouse = true;
    float m_DT{};
    uint32_t width{}, height{};
    int lastX{}, lastY{};

    Ref<Shader> basicShader;
    Ref<Shader> flatShader;
    Object3D object;
    Camera camera;
    Light light;
    Ref<Texture> texture0;
    Ref<Texture> texture1;
    Ref<Texture> texture2;
    Ref<Texture> texture3;
    Ref<Texture> texture4;
    Ref<Framebuffer> framebuffer;
    Window* m_Window;
};


