#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "Window.h"
#include <Renderer/GraphicsContext.h>
#include <Renderer/Renderer.h>

void GLAPIENTRY
MessageCallback( GLenum source,
                 GLenum type,
                 GLuint id,
                 GLenum severity,
                 GLsizei length,
                 const GLchar* message,
                 const void* userParam )
{
    if(severity!=GL_DEBUG_SEVERITY_NOTIFICATION)
        fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
             ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
             type, severity, message );
}


void ErrorCallback(int, const char* err_str)
{
    std::cout << "GLFW Error: " << err_str << std::endl;
}

Window::Window(const std::string& Title,int width,int height,int scaling, int argc, char** argv)
{
    this->width = width;
    this->height = height;

    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API,GLFW_OPENGL_API);
   // glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);
   // glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
   // glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    m_Window = glfwCreateWindow(width,height,Title.c_str(),nullptr,nullptr);
    if (this->Good())
    {
        std::cout << "Failed to create GLFW window!\n";
        glfwTerminate();
        exit(-1);
    }
    
    m_Context = Renderer::CreateGraphicsContext(m_Window);
    m_Context->Init();
    
    glEnable              ( GL_DEBUG_OUTPUT );
    glDebugMessageCallback( MessageCallback, 0 );

    glfwSetErrorCallback(ErrorCallback);
    glfwSetWindowCloseCallback(this->m_Window,closeCallback);
    glfwSetWindowSizeCallback(this->m_Window,windowSizeCallback);
    glfwSetKeyCallback(this->m_Window,windowKeyCallback);
    glfwSetCursorPosCallback(this->m_Window, windowMouseMoveCallback);
    glfwSetMouseButtonCallback(this->m_Window, windowMouseButtonCallback);
    glfwSetCursorPosCallback(this->m_Window, windowCursorMoveCallback);
    glfwSetScrollCallback(this->m_Window, windowScrollCallback);
//    glfwSetInputMode(this->m_Window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Window::Loop()
{
    this->width = s_ViewportSize.width;
    this->height = s_ViewportSize.height;;
}

void Window::Update()
{
    m_Context->SwapBuffers();
}

void Window::Clear(float r, float g, float b, float a)
{
    Renderer::ClearColor(glm::vec4{ r,g,b,a });
}
