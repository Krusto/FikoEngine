#pragma once
#include <string>
#include <Core/Window.h>
#include <Core/Ref.h>
class Application {
public:
    Application(std::string Name);
    bool Run();
    void Init();

    uint32_t GetWidth() { return m_WindowWidth; }
    uint32_t GetWidth() const { return m_WindowWidth; }
    uint32_t GetHeight() { return m_WindowHeight; }
    uint32_t GetHeight() const { return m_WindowHeight; }
private:
    Ref<Window> m_Window;
    uint32_t m_WindowWidth, m_WindowHeight;
    std::string m_AppName;
};