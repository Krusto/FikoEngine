#include <iostream>
#include <Core/DeltaTime.h>
#include <Editor/EditorLayer.h>
#include <ImGUI/ImGUILayer.h>
#include "Application.h"
#include <Sandbox/SandboxLayer.h>

bool Application::Run()
{
    m_Window = Ref<Window>::Create(m_AppName, m_WindowWidth, m_WindowHeight);
	
	Renderer::Init();
	
	auto imguiLayer = Ref<ImGUILayer>::Create();
	auto editorLayer = Ref<EditorLayer>::Create();
	auto sandboxLayer = Ref<SandboxLayer>::Create();
	LayerStack::PushLayer(imguiLayer);
	LayerStack::PushLayer(sandboxLayer);
	//LayerStack::PushLayer(editorLayer);

	for (auto layer : LayerStack::data()) {
		layer.second->Init(m_Window.Raw());
	}

	bool shouldExit = false;

	DeltaTime timer; float dt = 0;
	while (!shouldExit || !m_Window->ShouldClose()) {
		((ImGUILayer*)LayerStack::GetLayer("IMGUI LAYER"))->Begin();
		for (auto& [layerName, layer] : LayerStack::data()) {
			layer->OnImGuiDraw();
			layer->OnUpdate(dt);
			if (layer->ShouldExit()) {
				shouldExit = true;
				break;
			}
			dt = timer.Mark();
		}
		((ImGUILayer*)LayerStack::GetLayer("IMGUI LAYER"))->End();
		m_Window->Update();

	}
	for (auto& layer : LayerStack::data()) {
		layer.second->Destroy();
	}
	LayerStack::PopLayer("SandboxLayer");
	//LayerStack::PopLayer("Editor");
	LayerStack::PopLayer("IMGUI LAYER");

    m_Window->Close();
    return true;
}

Application::Application(std::string Name)
{
    this->m_AppName = Name;
    Init();
}

void Application::Init()
{
    m_WindowWidth = 1280;
    m_WindowHeight = 720;
}

