﻿#pragma once
#include <chrono>
using namespace std::chrono;
class DeltaTime {
public:
	DeltaTime() {
		last = high_resolution_clock::now();
	}
	float Mark() {
		const auto old = last;
		last = high_resolution_clock::now();
		using ms = std::chrono::duration<float, std::milli>;
		return std::chrono::duration_cast<ms>(last-old).count();
	}
private:
	steady_clock::time_point last;
};