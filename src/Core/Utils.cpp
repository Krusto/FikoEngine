﻿#include "Utils.h"

template<typename T>
inline bool Contains(const std::vector<T>& arr, const T& value)
{
	return std::find(arr.begin(), arr.end(), value) != arr.end();
}
template<typename T>
inline std::vector<T, std::allocator<T>>::iterator Find(std::vector<T, std::allocator<T>>& arr, const T& value)
{
	return std::find(arr.begin(), arr.end(), value);
}
template<typename T>
inline std::vector<T, std::allocator<T>>::const_iterator Find(std::vector<T, std::allocator<T>>& arr, const T& value)
{
	return std::find(arr.begin(), arr.end(), value);
}
