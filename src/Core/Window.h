#pragma once
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <string>
#include <Core/Ref.h>
#include <Layer/LayerStack.h>
#include <Renderer/GraphicsContext.h>
#include <Renderer/Camera.h>

class Window : public RefCounted{
public:
    static inline ViewportSize s_ViewportSize{};
    Window() = default;
    Window(const std::string& Title, int width,int height,int scaling = 0, int argc = 0, char** argv = nullptr);

    ~Window() = default;

    static Window Create(const std::string& Title, int width,int height,int scaling = 0, int argc = 0, char** argv = nullptr){return Window(Title,width,height,scaling,argc,argv);}

    bool ShouldClose(){return glfwWindowShouldClose(m_Window);}
    void Loop();
    [[nodiscard]] double getDeltaTime() const { return 1.0/m_Timestep; }

    void SetDeltaTime(double value) {
        m_Timestep = value;
    }

    GLFWwindow* GetHandle() { return m_Window; }
    [[nodiscard]] const auto& GetHandle() const{ return m_Window; }

    uint32_t width{},height{};

    bool Good(){return m_Window == NULL;}

    void Close(){ glfwSetWindowShouldClose(m_Window,1);}

    void Update();

    void Clear(float r = 0.2f, float g = 0.3f, float b = 0.4f, float a = 1.0f);
    explicit operator bool(){
        return (m_Window == nullptr);
    }
private:
    static void closeCallback(GLFWwindow* window){
        for (auto& [layerName, layer] : LayerStack::data()) {
            layer->OnWindowShouldCloseEvent();
            layer->SetShouldExit(true);
        }
        glfwSetWindowShouldClose(window,true);
    }
    static void windowSizeCallback(GLFWwindow* window, int width, int height)
    {
        s_ViewportSize.width = width;
        s_ViewportSize.height = height;
        for(auto& [layerName,layer] : LayerStack::data()){
            layer->OnWindowResizeEvent(width,height);
        }
    }
    static void windowKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){
        for(auto& [layerName,layer] : LayerStack::data()){
            layer->OnKeyboardEvent(action,key);
        }
    }
    static void windowMouseMoveCallback(GLFWwindow* window, double x,double y){
        for(auto& [layerName,layer] : LayerStack::data()){
            layer->OnMouseMoveEvent((int)x,(int)y);
        }
    }
    static void windowMouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
        for (auto& [layerName, layer] : LayerStack::data()) {
            layer->OnMouseButtonEvent(button,action,mods);
        }
    }
    static void windowCursorMoveCallback(GLFWwindow* window, double xpos, double ypos) {
        for (auto& [layerName, layer] : LayerStack::data()) {
            layer->OnCursorMoveEvent(xpos,ypos);
        }
    }

    static void windowScrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
        for (auto& [layerName, layer] : LayerStack::data()) {
            layer->OnScrollEvent(xoffset, yoffset);
        }
    }

    GLFWwindow* m_Window = nullptr;
    Ref<GraphicsContext> m_Context;
    double m_Timestep{};
};