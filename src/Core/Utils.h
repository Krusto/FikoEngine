﻿#pragma once
#include <vector>

template<typename T>
bool Contains(const std::vector<T>& arr,const T& value);
template<typename T>
std::vector<T,std::allocator<T>>::iterator Find(std::vector<T, std::allocator<T>>& arr, const T& value);
template<typename T>
std::vector<T,std::allocator<T>>::const_iterator Find(std::vector<T, std::allocator<T>>& arr, const T& value);
