#pragma once
#include <Scene/Component.h>
#include <Scene/Entity.h>
#include <Scene/Scene.h>

DrawableComponent DrawableComponent::Generate(Entity entity) {
	const auto& mesh = entity.GetComponent<MeshComponent>().mesh;

	DrawableComponent output;
	output.va = VertexArray::Create((uint32_t)mesh.indices.size());
	output.va->Bind();
	auto vb = VertexBuffer::Create(output.va, Vertex::GetLayout(), (float*)mesh.vertices.data(), (uint32_t)mesh.vertices.size());
	auto ib = IndexBuffer::Create(output.va, mesh.indices.data(), (uint32_t)mesh.indices.size());
	output.va->Unbind();

	return output;
}

MeshComponent MeshComponent::Generate(Entity entity, MeshType type,QuadSide side)
{
	const auto& mesh = MeshBuilder::CreateMesh(type, side);

	MeshComponent output;
	output.type = type;

	output.va = VertexArray::Create((uint32_t)mesh.indices.size());
	output.va->Bind();
	auto vb = VertexBuffer::Create(output.va, Vertex::GetLayout(), (float*)mesh.vertices.data(), (uint32_t)mesh.vertices.size());
	auto ib = IndexBuffer::Create(output.va, mesh.indices.data(), (uint32_t)mesh.indices.size());
	output.va->Unbind();

	return output;
}

MeshComponent MeshComponent::Generate(Entity entity,const Mesh& mesh)
{
	MeshComponent output;
	output.type = MeshType::Custom;

	output.va = VertexArray::Create((uint32_t)mesh.indices.size());
	output.va->Bind();
	auto vb = VertexBuffer::Create(output.va, Vertex::GetLayout(), (float*)mesh.vertices.data(), (uint32_t)mesh.vertices.size());
	auto ib = IndexBuffer::Create(output.va, mesh.indices.data(), (uint32_t)mesh.indices.size());
	output.va->Unbind();

	return output;
}

MeshListComponent& MeshListComponent::AddMesh(glm::ivec3 position,const Mesh& mesh)
{

	data.emplace(std::make_pair(position, nullptr));

	data[position] = VertexArray::Create((uint32_t)mesh.indices.size());
	data[position]->Bind();
	auto vb = VertexBuffer::Create(data[position], Vertex::GetLayout(), (float*)mesh.vertices.data(), (uint32_t)mesh.vertices.size() * sizeof(float));
	auto ib = IndexBuffer::Create(data[position], mesh.indices.data(), (uint32_t)mesh.indices.size());
	data[position]->Unbind();

	return *this;
}

MeshListComponent& MeshListComponent::RemoveMesh(const glm::ivec3& position)
{
	if (data.contains(position))
		data.erase(position);
	return *this;
}
