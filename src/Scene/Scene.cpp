#include "Scene.h"
#include <Scene/Entity.h>
#include <Scene/Component.h>
#include <Core/UUID.h>
Scene::Scene()
{
    m_SceneEntity = m_Registry.create();
    Entity(m_SceneEntity,this).AddComponent<IDComponent>();
    Entity(m_SceneEntity, this).GetComponent<IDComponent>().ID = m_SceneID;
    Entity(m_SceneEntity, this).AddComponent<RelationshipComponent>();
    Entity(m_SceneEntity, this).AddComponent<TagComponent>();
    Entity(m_SceneEntity, this).GetComponent<TagComponent>().Tag = "Scene";

}
void Scene::OnUpdate(float dt)
{
}

Entity Scene::AddEntity(std::string name)
{
    Entity entity = Entity{ m_Registry.create(), this };
    entity.AddComponent<IDComponent>();
    entity.AddComponent<TagComponent>();
    entity.AddComponent<RelationshipComponent>();

    UUID id;
    entity.GetComponent<IDComponent>().ID = id;
    entity.GetComponent<TagComponent>().Tag = name;
    
    entity.GetComponent<RelationshipComponent>().ParentHandle = m_SceneID;

    Entity(m_SceneEntity, this).AddChild(entity);
    m_EntityMap.emplace(std::pair(id,entity));
    return entity;
}

Entity Scene::CreateEntity(std::string name)
{
    Entity entity = Entity{ m_Registry.create(), this };
    entity.AddComponent<IDComponent>();
    entity.AddComponent<TagComponent>();
    entity.AddComponent<RelationshipComponent>();

    UUID id;
    entity.GetComponent<IDComponent>().ID = id;
    entity.GetComponent<TagComponent>().Tag = name;

    m_EntityMap.emplace(std::pair(id, entity));
    return entity;
}

Entity Scene::RemoveEntity(UUID uuid)
{
    if (HasUUID(uuid)) {
        m_Registry.destroy(m_EntityMap[uuid]);
        m_EntityMap.erase(uuid);
    }
    return { m_SceneEntity,this };
}

Entity Scene::RemoveEntity(Entity entity)
{
    auto uuid = entity.GetUUID();
    if (HasUUID(uuid)) {
        m_Registry.destroy(m_EntityMap[uuid]);
        m_EntityMap.erase(uuid);
    }
    return { m_SceneEntity,this };
}

Entity Scene::GetHandle()
{
    return { (uint32_t)m_SceneEntity,this };
}

Entity Scene::GetEntity(UUID uuid)
{
    if(m_EntityMap.contains(uuid))
        return m_EntityMap.at(uuid);
    return Entity{ (uint32_t)entt::null ,this };
}

Entity Scene::FindEntity(const std::string& name)
{
    for (auto& [uuid, entity] : m_EntityMap) {
        if (entity.HasComponent<TagComponent>()) {
            if (entity.GetComponent<TagComponent>().Tag == name) {
                return entity;
            }
        }
    }
    return { (uint32_t)m_SceneEntity,this };
}

Entity Scene::GetSelectedEntity()
{
    if(m_SelectedEntity != entt::null)
        return Entity{ (uint32_t)m_SelectedEntity,this }; 
    return {(uint32_t)entt::null,this};
}

Entity Scene::SetSelectedEntity(const Entity& entity)
{
    m_SelectedEntity = (entt::entity)entity;
    return GetHandle();
}
