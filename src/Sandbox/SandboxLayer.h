﻿#pragma once
#include <Layer/Layer.h>
#include <Core/Window.h>
#include <Core/DeltaTime.h>
#include <Renderer/Renderer.h>
#include <Scene/Scene.h>
#include <Renderer/Shader.h>
#include "Player.h"
#include <Sandbox/World/World.h>
class SandboxLayer : public Layer {
public:
    SandboxLayer();
    void Init(Window* window) override;
    void OnAttach() override;
    void OnDetach() override;
    void OnUpdate(float dt) override;
    void Destroy() override {};
    void OnImGuiDraw() override;
    void OnMouseMoveEvent(int width, int height) override;
    void OnKeyboardEvent(int action, int key) override;
    void OnWindowResizeEvent(int width, int height) override;
    void OnWindowShouldCloseEvent() override { SetShouldExit(true); window->Close(); };
    void OnMouseButtonEvent(int button, int action, int mods) override;
    void OnCursorMoveEvent(double xpos, double ypos) override;
    void OnScrollEvent(double xoffset, double yoffset) override;
protected:
    Window* window = nullptr;
    ViewportSize windowSize;

    Scene* m_CurrentScene;

    Ref<Framebuffer> m_Framebuffer;
    Ref<Shader> m_WorldShader;
    Ref<Shader> m_StandardShader;
    Ref<Material> m_ExampleMaterial;

    ViewportSize m_ViewportSize;

    Player m_Player;
    DeltaTime timer;
    float deltaTime;

    bool isScrollPressed{}, isScrollReleased{};
    bool isRotationEnabled{};

    glm::vec3 lightPos{};

    Ref<World> myWorld;
    TransformComponent m_ChunkTransform;
    Mesh cubeMesh;
};