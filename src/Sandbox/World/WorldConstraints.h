﻿#pragma once
#include <cstdint>

static constexpr uint8_t SECTION_SIZE = 16;
static constexpr int SECTION_AREA = 16 * 16;
static constexpr int SECTION_CUBED = 16 * 16 * 16;
static constexpr uint8_t CHUNK_SIZE_X = 16;
static constexpr uint8_t CHUNK_SIZE_Z = 16;
static constexpr int CHUNK_AREA = 16 * 16;
static constexpr int WATER_LEVEL = 32;

static constexpr uint32_t MaxChunkDataBuildCountPerFrame = 200;
static constexpr uint32_t MaxChunkMeshBuildCountPerFrame = 200;

static constexpr int TICKRATE = 60;
static constexpr int DAY_TICKS = (14 * 60 * TICKRATE);
static constexpr int  NIGHT_TICKS = (10 * 60 * TICKRATE);
static constexpr int  TOTAL_DAY_TICKS = (DAY_TICKS + NIGHT_TICKS);

static constexpr bool GENERATE_WORLD_ASYNC = true;