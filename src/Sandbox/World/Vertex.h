﻿#pragma once

#include <glm/gtx/string_cast.hpp>
#include <Renderer/VertexLayout.h>

struct VertexData {
public:
    VertexData() = default;
    VertexData(uint32_t TextureCoordIndex, uint8_t Face, uint8_t BlockID) {
        data = (float)((uint32_t)(TextureCoordIndex) | (uint32_t)(Face<<8) | (uint32_t)(BlockID<<16));//(((uint8_t)TextureCoordIndex));
    }

    friend VertexData operator+(VertexData lhs,        // passing lhs by value helps optimize chained a+b+c
        const VertexData rhs) // otherwise, both parameters may be const references
    {
        lhs.data = (float)((uint32_t)lhs.data | (uint32_t)rhs.data);
        return lhs; // return the result by value (uses move constructor)
    }
    float data{};
};
struct BlockVertex{
public:
    float PositionInChunk;
    VertexData data{};
    glm::vec3 Position{};
    BlockVertex(glm::vec3 Position,VertexData data,float PositionInChunk = 0)
        :Position(Position),data(data),PositionInChunk(PositionInChunk){}
    static auto GetVertexSize() {
        return GetLayout().stride;
    }
    static VertexLayout GetLayout() {
        return VertexLayout({ VertexAttribute{.name = "PositionInChunk",.type = "float"},VertexAttribute{.name="data",.type="float"},VertexAttribute{.name = "Position",.type = "vec3"} });
    }
};
