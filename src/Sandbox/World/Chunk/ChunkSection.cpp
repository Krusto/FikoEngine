#include <Sandbox/World/Chunk/ChunkSection.h>
#include <Sandbox/World/WorldConstraints.h>
ChunkSection::ChunkSection()
{
}

void ChunkSection::SetBlock(glm::lowp_u8vec3 Position, Block block)
{
	if (Position.x < SECTION_SIZE && Position.x >= 0 &&
		Position.y < SECTION_SIZE && Position.y >= 0 &&
		Position.z < SECTION_SIZE && Position.z >= 0
		) {
		m_Blocks[Position.x][Position.y][Position.z] = block;
	}
}

Block ChunkSection::GetBlock(glm::lowp_u8vec3 Position)
{

	if (Position.x < SECTION_SIZE && Position.x >= 0 &&
	   Position.y < SECTION_SIZE && Position.y >= 0 &&
	   Position.z < SECTION_SIZE && Position.z >= 0
	) {
		return m_Blocks[Position.x][Position.y][Position.z];
	}
	return Block::Air;
}

uint32_t ChunkSection::FlatCoordinate(glm::lowp_u8vec3 Position)
{
	
	return (size_t)(SECTION_AREA * (size_t)Position.z + SECTION_SIZE * (size_t)Position.y + (size_t)Position.x);
}
uint32_t ChunkSection::FlatCoordinate(glm::lowp_u8vec3 Position) const
{
	return (size_t)(SECTION_AREA * (size_t)Position.z + SECTION_SIZE * (size_t)Position.y + (size_t)Position.x);
}
