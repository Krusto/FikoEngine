﻿//#pragma once
//#include <glm/glm.hpp>
//#include <Scene/Scene.h>
//#include <Scene/Entity.h>
//#include <Sandbox/World/Chunk.h>
//#include <Sandbox/Player.h>
//
//class ChunkController {
//public:
//	ChunkController() = default;
//	ChunkController(Scene* scene, uint32_t seed, uint32_t renderdistance);
//	void GetChunksInRenderDistance(glm::vec3 PlayerPosition);
//	void QueueChunkDataBuilding();
//	void QueueChunkMeshBuilding();
//	void QueueOutChunkDataRangeDataUnloading(glm::ivec3 centerChunkPosition);
//	void QueueOutChunkMeshRangeDataUnloading(glm::ivec3 centerChunkPosition);
//	void BuildChunkDataInQueue();
//	void BuildChunkMeshInQueue();
//	void UnloadOutOfRangeData();
//	void UnloadOutOfRangeMesh();
//	void UploadChunkMeshQueue();
//	bool HasChunksToBuild();
//	bool HasChunksToUnload();
//
//	Block GetBlock(glm::vec3 position);
//	Ref<ChunkData> GetChunk(glm::ivec3 position);
//	Ref<ChunkData> GetChunk(Player& player);
//	
//	bool IsPlayerColliding(Player& player,glm::vec3 direction,Block block);
//
//private:
//	void BuildChunkData(const glm::ivec3& ChunkPosition);
//	static ChunkData BuildChunkDataAsync(glm::ivec3 ChunkPosition,uint32_t seed);
//	static void BuildChunkDataAsyncThreaded(ChunkData* data,glm::ivec3 ChunkPosition,uint32_t seed);
//
//	void BuildChunkMesh(glm::ivec3 ChunkPosition);
//	static Ref<ChunkMesh> BuildChunkMeshAsync(Ref<ChunkData> data);
//	void UnloadChunkData(glm::ivec3 ChunkPosition);
//	void UnloadChunkMesh(glm::ivec3 ChunkPosition);
//
//	void GetChunksDataOutsideRenderDistance(glm::ivec3 centerChunkPosition);
//	void GetChunksMeshOutsideRenderDistance(glm::ivec3 centerChunkPosition);
//	std::pair<bool,Ref<ChunkData>> GetChunkData(glm::ivec3 ChunkPosition);
//
//	Scene* m_Scene{};
//	Entity m_WorldEntity{};
//	uint32_t m_Seed{};
//	uint32_t m_RenderDistance{};
//
//	std::vector<glm::ivec3> m_ChunksInRenderDistance;
//	std::vector<glm::ivec3> m_ChunkDataOutsideRenderDistance;
//	std::vector<glm::ivec3> m_ChunkMeshOutsideRenderDistance;
//	
//	std::vector<glm::ivec3> m_GeneratedChunkData;
//	std::vector<glm::ivec3> m_GeneratedChunkMeshes;
//	std::vector<glm::ivec3> m_ChunkDataBuildQueue;
//	std::vector<glm::ivec3> m_ChunkMeshBuildQueue;
//	std::vector<glm::ivec3> m_UploadChunkMeshQueue;
//
//	std::vector<Ref<ChunkData>> m_ChunkDataList;
//	std::vector<Ref<ChunkMesh>> m_ChunkMeshList;
//};