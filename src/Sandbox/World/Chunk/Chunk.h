﻿#pragma once
#include <glm/glm.hpp>
#include <unordered_map>
#include <Core/Ref.h>
#include <Sandbox/World/Chunk/Block.h>
#include <Sandbox/World/Chunk/ChunkSection.h>
#define Flat(position) static_cast<uint16_t>((position.x | (position.y << 8)))
class Chunk : public RefCounted{
public:
	Chunk() = default;
	Chunk(glm::ivec3 Position);

	void SetBlock(glm::ivec3 Position, Block block);
	Block GetBlock(glm::ivec3 Position);

	uint32_t GetLayerCount() const;
	uint32_t GetHeight() const;
	bool IsGenerated() const;

	auto& GetData() const;
	auto& GetData();

	glm::ivec3 GetPosition();
	glm::ivec3 GetPosition() const;

	void SetIsGenerated(bool flag);
private:
	//std::unordered_map<uint8_t, ChunkSection> m_Data;
	//ChunkSection m_Data;
	Block m_Data[SECTION_SIZE][SECTION_SIZE][SECTION_SIZE];
	bool m_IsGenerated{};
	glm::ivec3 m_Position{};
};