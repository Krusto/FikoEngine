﻿//#include "ChunkController.h"
//#include <Core/Utils.h>
//#include <Core/Utils.cpp>
//#include <Core/Ref.h>
//#include <Sandbox/World/WorldConstraints.h>
//#include <Core/DeltaTime.h>
//#include <iostream>
//#include <thread>
//#include <future>
//#include <Sandbox/Player.h>
//ChunkController::ChunkController(Scene* scene, uint32_t seed, uint32_t renderdistance)
//:m_Scene(scene), m_Seed(seed), m_RenderDistance(renderdistance) {
//	m_WorldEntity = scene->FindEntity("World");
//}
//
//void ChunkController::GetChunksInRenderDistance(glm::vec3 PlayerPosition)
//{
//	m_ChunksInRenderDistance.clear();
//	for (size_t i = 0; i < m_RenderDistance; i++)
//	{
//		for (size_t j = 0; j < m_RenderDistance; j++)
//		{
//			m_ChunksInRenderDistance.emplace_back(glm::ivec3{   (uint64_t)glm::floor(PlayerPosition.x)+i-m_RenderDistance/2,
//																0,
//																(uint64_t)glm::floor(PlayerPosition.z)+j-m_RenderDistance/2});
//		}
//	}
//}
//
//void ChunkController::QueueChunkDataBuilding()
//{
//	for (auto& chunkInRenderDistance : m_ChunksInRenderDistance) {
//		if (!Contains(m_GeneratedChunkData, chunkInRenderDistance)) {
//			m_ChunkDataBuildQueue.emplace_back(chunkInRenderDistance);
//		}
//	}
//}
//
//void ChunkController::QueueChunkMeshBuilding()
//{
//	for (auto& chunkInRenderDistance : m_ChunksInRenderDistance) {
//		if (!Contains(m_GeneratedChunkMeshes, chunkInRenderDistance) && 
//			 Contains(m_GeneratedChunkData,chunkInRenderDistance)) {
//			m_ChunkMeshBuildQueue.emplace_back(chunkInRenderDistance);
//		}
//	}
//}
//
//void ChunkController::QueueOutChunkDataRangeDataUnloading(glm::ivec3 centerChunkPosition)
//{
//	GetChunksDataOutsideRenderDistance(centerChunkPosition);
//}
//
//void ChunkController::QueueOutChunkMeshRangeDataUnloading(glm::ivec3 centerChunkPosition)
//{
//	GetChunksMeshOutsideRenderDistance(centerChunkPosition);
//}
//
//void ChunkController::BuildChunkDataInQueue()
//{
//	std::vector<std::future<Ref<ChunkData>>> threads;
//	//std::vector<std::thread> threads;
//	size_t i{};
//	if (m_ChunkDataBuildQueue.size() <= MaxChunkDataBuildCountPerFrame) {
//		//m_ChunkDataList.resize(m_ChunkDataBuildQueue.size());
//	}
//	else {
//	//	m_ChunkDataList.resize(MaxChunkDataBuildCountPerFrame);
//	}
//
//	for (i = 0; i < MaxChunkDataBuildCountPerFrame; i++)
//	{
//		if (i < m_ChunkDataBuildQueue.size()) {
//			m_GeneratedChunkData.emplace_back(m_ChunkDataBuildQueue[i]);
//		
//			auto position = m_ChunkDataBuildQueue[i];
//			//if (GENERATE_WORLD_ASYNC) {
//				m_ChunkDataList.emplace_back(Ref<ChunkData>::Create(position, m_Seed));
//		//	threads.emplace_back(std::async(std::launch::async, ChunkController::BuildChunkDataAsync
//			//						, position, m_Seed));
//			//}
//			//else {
//			//	threads.emplace_back(std::async(std::launch::deferred, &ChunkController::BuildChunkDataAsync
//			//		, position, m_Seed));
//			//}
//		}
//		else {
//			break;
//		}
//	}
//	m_ChunkDataBuildQueue.erase(m_ChunkDataBuildQueue.begin(),m_ChunkDataBuildQueue.begin()+i);
//	m_ChunkDataBuildQueue.shrink_to_fit();
//	for (auto& thread : threads)
//	{
//		thread.wait();
//	}
//	for (auto& thread : threads)
//	{
//		m_ChunkDataList.emplace_back(thread.get());
//	}
//}
//
//void ChunkController::BuildChunkMeshInQueue()
//{
//	std::vector<std::future<Ref<ChunkMesh>>> threads;
//	size_t i{};
//	for (i = 0; i < MaxChunkMeshBuildCountPerFrame; i++)
//	{
//		if(i < m_ChunkMeshBuildQueue.size()){
//			//if (GENERATE_WORLD_ASYNC) {
//				auto [generated,data] = GetChunkData(m_ChunkMeshBuildQueue[i]);
//				//if (generated) {
//					threads.emplace_back(std::async(std::launch::async,
//						ChunkController::BuildChunkMeshAsync,
//						data));
//				//}
//			//}
//			//else {
//				//threads.emplace_back(std::async(std::launch::deferred, 
//				//								ChunkController::BuildChunkMeshAsync,
//				//								GetChunkData(m_ChunkMeshBuildQueue[i])));
//			//}
//		 	//m_ChunkMeshList.emplace_back(Ref<ChunkMesh>::Create(data, nullptr));
//			m_UploadChunkMeshQueue.emplace_back(m_ChunkMeshBuildQueue[i]);
//			m_GeneratedChunkMeshes.emplace_back(m_ChunkMeshBuildQueue[i]);
//		}
//		else {
//			break;
//		}
//	}
//	m_ChunkMeshBuildQueue.erase(m_ChunkMeshBuildQueue.begin(),m_ChunkMeshBuildQueue.begin()+i);
//	m_ChunkMeshBuildQueue.shrink_to_fit();
//	for (auto& thread : threads)
//	{
//		thread.wait();
//	}
//	for (auto& thread : threads)
//	{
//		auto mesh = thread.get();
//		if(GetMesh(m_ChunkMeshList,mesh->GetPosition()) == nullptr)
//			m_ChunkMeshList.emplace_back(mesh);
//	}
//}
//
//void ChunkController::UnloadOutOfRangeData()
//{
//	for (auto& position : m_ChunkDataOutsideRenderDistance)
//	{
//		this->UnloadChunkData(position);
//	}
//}
//
//void ChunkController::UnloadOutOfRangeMesh()
//{
//	for (auto& position : m_ChunkMeshOutsideRenderDistance)
//	{
//		this->UnloadChunkMesh(position);
//	}
//}
//
//void ChunkController::UploadChunkMeshQueue()
//{
//	uint32_t i = 0;
//	for (auto& position : m_UploadChunkMeshQueue)
//	{
//		auto mesh = GetMesh(m_ChunkMeshList, position);
//		if(mesh != nullptr)
//			m_WorldEntity.GetComponent<ChunkMeshListComponent>().AddMesh(position, mesh);
//	}
//	m_ChunkMeshList.clear();
//	m_ChunkMeshList.shrink_to_fit();
//	m_UploadChunkMeshQueue.clear();
//}
//
//bool ChunkController::HasChunksToBuild()
//{
//	return m_UploadChunkMeshQueue.size() > 0 || m_ChunkDataBuildQueue.size() > 0 || m_ChunkMeshBuildQueue.size() > 0;
//}
//
//bool ChunkController::HasChunksToUnload()
//{
//	return m_ChunkDataOutsideRenderDistance.size() > 0 || m_ChunkMeshOutsideRenderDistance.size() > 0;
//}
//
//Block ChunkController::GetBlock(glm::vec3 position)
//{
//
//	glm::ivec3 ChunkPosition = { position.x / CHUNK_SIZE_X,position.y / CHUNK_SIZE_Y,position.z / CHUNK_SIZE_Z };
//	
//	if (Contains(m_GeneratedChunkData, ChunkPosition)) {
//		glm::ivec3 localPosition = {
//		glm::abs(glm::floor(position.x) - ChunkPosition.x * CHUNK_SIZE_X) ,
//		glm::abs(glm::floor(position.y) - ChunkPosition.y * CHUNK_SIZE_Y) ,
//		glm::abs(glm::floor(position.z) - ChunkPosition.z * CHUNK_SIZE_Z)
//		};
//		return GetData(m_ChunkDataList, ChunkPosition)->GetBlock(localPosition);
//	}
//
//	return Block::Air;
//}
//
//Ref<ChunkData> ChunkController::GetChunk(glm::ivec3 position)
//{
//	return GetData(m_ChunkDataList, position);
//}
//
//Ref<ChunkData> ChunkController::GetChunk(Player& player)
//{
//	glm::ivec3 playerPosition = player.GetPositionI();
//	glm::ivec3 chunkPosition = glm::ivec3((int)(playerPosition.x/CHUNK_SIZE_X),0,(int)(playerPosition.z/CHUNK_SIZE_X));
//
//	return GetData(m_ChunkDataList, chunkPosition);
//}
//
//bool ChunkController::IsPlayerColliding(Player& player, glm::vec3 direction, Block block)
//{
//	auto playerPosition = player.GetPosition();
//	float playerWidth = 0.6f;
//	float playerHalfWidth = 0.3f;
//	float playerHeight = 1.8f;
//	float playerHalfHeight = 0.9f;
//
//	glm::vec3 playerMin = glm::vec3(playerPosition.x, playerPosition.y, playerPosition.z);
//	glm::vec3 playerMax = glm::vec3(
//		playerPosition.x + playerWidth,
//		playerPosition.y + playerHeight,
//		playerPosition.z + playerWidth);
//
//	glm::vec3 boxMin = player.GetPosition() + direction;
//	glm::vec3 boxMax = boxMin + glm::vec3(1, 1, 1);
//	
//	if (
//		playerMin.x <= boxMax.x && playerMax.x >= boxMin.x &&
//		playerMin.y <= boxMax.y && playerMax.y >= boxMin.y &&
//		playerMin.z <= boxMax.z && playerMax.z >= boxMin.z
//		) {
//		Block intersectedBlock = GetBlock(boxMin);
//		if (intersectedBlock == block) {
//			return true;
//		}
//	}
//
//	return false;
//}
//
//void ChunkController::BuildChunkData(const glm::ivec3& ChunkPosition)
//{
//	m_ChunkDataList.emplace_back(Ref<ChunkData>::Create(ChunkPosition,m_Seed));
//}
//
//ChunkData ChunkController::BuildChunkDataAsync(glm::ivec3 ChunkPosition,uint32_t seed)
//{
//	return ChunkData(ChunkPosition, seed);
//}
//
//void ChunkController::BuildChunkDataAsyncThreaded(ChunkData* data, glm::ivec3 ChunkPosition, uint32_t seed)
//{
//	data->Generate(ChunkPosition, seed);
//}
//
//void ChunkController::BuildChunkMesh(glm::ivec3 ChunkPosition)
//{
//	auto data = GetData(m_ChunkDataList, ChunkPosition);
//	m_ChunkMeshList.emplace_back(Ref<ChunkMesh>::Create(data, nullptr));
//	m_UploadChunkMeshQueue.emplace_back(ChunkPosition);
//}
//
//Ref<ChunkMesh> ChunkController::BuildChunkMeshAsync(Ref<ChunkData> data)
//{
//	return Ref<ChunkMesh>::Create(data, nullptr);
//}
//
//void ChunkController::UnloadChunkData(glm::ivec3 ChunkPosition)
//{
//	if (Contains(m_GeneratedChunkData, ChunkPosition)) {
//		m_GeneratedChunkData.erase(std::ranges::find(m_GeneratedChunkData,ChunkPosition));
//		for (size_t i = 0; i < m_ChunkDataList.size(); i++)
//		{
//			if (m_ChunkDataList[i]->GetPosition() == ChunkPosition) {
//				m_ChunkDataList.erase(m_ChunkDataList.begin() + i);
//				break;
//			}
//		}
//	}
//}
//void ChunkController::UnloadChunkMesh(glm::ivec3 ChunkPosition)
//{
//	if (Contains(m_GeneratedChunkMeshes, ChunkPosition)) {
//		m_GeneratedChunkMeshes.erase(std::ranges::find(m_GeneratedChunkMeshes, ChunkPosition));
//		m_WorldEntity.GetComponent<ChunkMeshListComponent>().RemoveMesh(ChunkPosition);
//	}
//}
//
//void ChunkController::GetChunksDataOutsideRenderDistance(glm::ivec3 centerChunkPosition)
//{
//	m_ChunkDataOutsideRenderDistance.clear();
//	for (auto& position : m_GeneratedChunkData) {
//		if (!Contains(m_ChunksInRenderDistance,position)) {
//				m_ChunkDataOutsideRenderDistance.emplace_back(position);
//		}
//	}
//}
//
//void ChunkController::GetChunksMeshOutsideRenderDistance(glm::ivec3 centerChunkPosition)
//{
//	m_ChunkMeshOutsideRenderDistance.clear();
//	for (auto& position : m_GeneratedChunkMeshes) {
//		if (!Contains(m_ChunksInRenderDistance, position)) {
//			m_ChunkMeshOutsideRenderDistance.emplace_back(position);
//		}
//	}
//}
//
//std::pair<bool, Ref<ChunkData>> ChunkController::GetChunkData(glm::ivec3 ChunkPosition)
//{
//	for (auto& chunk : m_ChunkDataList)
//	{
//		if (chunk->GetPosition() == ChunkPosition) {
//			return {true,chunk};
//		}
//	}
//	return { false,Ref<ChunkData>::Create() };
//}
