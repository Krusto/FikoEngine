﻿//#include "Chunk.h"
//#include <vector>
//#include <Renderer/VertexArray.h>
//#include <Renderer/VertexBuffer.h>
//#include <Renderer/IndexBuffer.h>
//#include <Sandbox/World/WorldConstraints.h>
//#include "./Generator/WorldGen.h"
//
//#define DEBUG_NEW new(__FILE__, __LINE__)
//#define new DEBUG_NEW
//
//ChunkData::ChunkData(glm::ivec3 ChunkPosition, uint64_t seed)
//{
//	Generate(ChunkPosition, seed);
//}
//
//void ChunkData::Generate(glm::ivec3 ChunkPosition, uint64_t seed)
//{
//	m_ChunkPosition = ChunkPosition;
//	m_Seed = seed;
//	m_Blocks.resize(CHUNK_SIZE_X * CHUNK_SIZE_Y * CHUNK_SIZE_Z);
//	WorldGen::Generate(*this, seed);
//	m_Generated = true;
//}
//
//Block ChunkData::GetBlock(glm::ivec3 Position)
//{
//	if (Position.x < CHUNK_SIZE_X && Position.x >= 0 &&
//		Position.y < CHUNK_SIZE_Y && Position.y >= 0 &&
//		Position.z < CHUNK_SIZE_Z && Position.z >= 0
//		) {
//		size_t index = (size_t)(CHUNK_SIZE_X*CHUNK_SIZE_Y * (size_t)Position.z + CHUNK_SIZE_X * (size_t)Position.y + (size_t)Position.x);
//		return m_Blocks[index];
//	}
//	return Block::Air;
//}
//const Block ChunkData::GetBlock(glm::ivec3 Position) const
//{
//	if (m_Blocks.size() > 0) {
//		if (Position.x < CHUNK_SIZE_X && Position.x >= 0 &&
//			Position.y < CHUNK_SIZE_Y && Position.y >= 0 &&
//			Position.z < CHUNK_SIZE_Z && Position.z >= 0
//			) {
//			size_t index = (size_t)(CHUNK_SIZE_X * CHUNK_SIZE_Y * (size_t)Position.z + CHUNK_SIZE_X * (size_t)Position.y + (size_t)Position.x);
//			return m_Blocks[index];
//		}
//	}
//	return Block::Air;
//}
//void ChunkData::SetBlock(glm::ivec3 Position, Block block)
//{
//	if (Position.x < CHUNK_SIZE_X && Position.x >= 0 &&
//		Position.y < CHUNK_SIZE_Y && Position.y >= 0 &&
//		Position.z < CHUNK_SIZE_Z && Position.z >= 0
//		) {
//		size_t index = (size_t)(CHUNK_SIZE_X * CHUNK_SIZE_Y * (size_t)Position.z + CHUNK_SIZE_X * (size_t)Position.y + (size_t)Position.x);
//		m_Blocks[index] = block;
//	}
//}
//
//void ChunkData::Clear()
//{
//	m_Blocks.clear();
//	m_Blocks.shrink_to_fit();
//}
//
//ChunkMesh::ChunkMesh(Ref<ChunkData> chunkData, void* NeighbourChunks)
//{
//	if (chunkData->IsGenerated()) {
//		this->m_ChunkPosition = chunkData->GetPosition();
//		this->m_NeighbourChunks = NeighbourChunks;
//
//		uint32_t maxVertices = CHUNK_CUBED * 8;
//		uint32_t maxIndices = CHUNK_CUBED * 6 * 8;
//
//		//vertices.reserve(maxVertices);
//		//indices.reserve(maxIndices);
//		for (uint32_t y = 0; y < CHUNK_SIZE_Y; y++)
//		{
//			for (uint32_t x = 0; x < CHUNK_SIZE_X; x++)
//			{
//				for (uint32_t z = 0; z < CHUNK_SIZE_Z; z++)
//				{
//					glm::ivec3 blockPosition = glm::ivec3{ x,y,z };
//					Block block = chunkData->GetBlock(blockPosition);
//
//					if (block == Block::Air) {
//						continue;
//					}
//
//					glm::ivec3 FRONT_BLOCK = glm::ivec3{ 0 , 0 , 1 } + blockPosition;
//					glm::ivec3 BACK_BLOCK = glm::ivec3{ 0 , 0 ,-1 } + blockPosition;
//					glm::ivec3 TOP_BLOCK = glm::ivec3{ 0 , 1 , 0 } + blockPosition;
//					glm::ivec3 BOTTOM_BLOCK = glm::ivec3{ 0 , -1 , 0 } + blockPosition;
//					glm::ivec3 LEFT_BLOCK = glm::ivec3{ 1 , 0 , 0 } + blockPosition;
//					glm::ivec3 RIGHT_BLOCK = glm::ivec3{ -1 , 0 , 0 } + blockPosition;
//
//					if (chunkData->GetBlock(TOP_BLOCK) == Block::Air || chunkData->GetBlock(TOP_BLOCK) == Block::None) {
//						AddBlockFace(TOP_FACE, blockPosition, block);
//					}
//					if (chunkData->GetBlock(BOTTOM_BLOCK) == Block::Air || chunkData->GetBlock(BOTTOM_BLOCK) == Block::None) {
//						AddBlockFace(BOTTOM_FACE, blockPosition, block);
//					}
//					if (chunkData->GetBlock(LEFT_BLOCK) == Block::Air || chunkData->GetBlock(LEFT_BLOCK) == Block::None) {
//						AddBlockFace(RIGHT_FACE, blockPosition, block);
//					}
//					if (chunkData->GetBlock(RIGHT_BLOCK) == Block::Air || chunkData->GetBlock(RIGHT_BLOCK) == Block::None) {
//						AddBlockFace(LEFT_FACE, blockPosition, block);
//					}
//					if (chunkData->GetBlock(FRONT_BLOCK) == Block::Air || chunkData->GetBlock(FRONT_BLOCK) == Block::None) {
//						AddBlockFace(FRONT_FACE, blockPosition, block);
//					}
//					if (chunkData->GetBlock(BACK_BLOCK) == Block::Air || chunkData->GetBlock(BACK_BLOCK) == Block::None) {
//						AddBlockFace(BACK_FACE, blockPosition, block);
//					}
//				}
//			}
//		}
//
//		vertices.shrink_to_fit();
//		indices.shrink_to_fit();
//	}
//}
//
//void ChunkMesh::Clear()
//{
//	vertices.clear();
//	indices.clear();
//	vertices.shrink_to_fit();
//	indices.shrink_to_fit();
//}
//
//void ChunkMesh::AddBlockFace( BlockFace_t blockFace, const glm::vec3& position, const Block& block)
//{
//	float packedPosition = (float)((uint32_t)position.x >> 16 | (uint32_t)position.y >> 8 | ( 0xFF & (uint32_t)position.z));
//
//	vertices.emplace_back(BlockVertex(blockFace[0].Position + position, blockFace[0].data + VertexData(0, 0, (uint8_t)block), packedPosition));
//	vertices.emplace_back(BlockVertex(blockFace[1].Position + position, blockFace[1].data +VertexData(0, 0, (uint8_t)block), packedPosition));
//	vertices.emplace_back(BlockVertex(blockFace[2].Position + position, blockFace[2].data +VertexData(0, 0, (uint8_t)block), packedPosition));
//	vertices.emplace_back(BlockVertex(blockFace[3].Position + position, blockFace[3].data+VertexData(0, 0, (uint8_t)block), packedPosition));
//		
//	indices.insert(indices.end(), { maxIndex,maxIndex + 1,maxIndex + 2,maxIndex + 2,maxIndex + 3,maxIndex });
//	maxIndex += 4;
//}
//
//Ref<ChunkData> GetData(std::vector<Ref<ChunkData>>& arr, glm::ivec3 position)
//{
//	for (auto& data : arr) {
//		if (data->GetPosition() == position) {
//			return data;
//		}
//	}
//	return nullptr;
//}
//Ref<ChunkMesh> GetMesh(std::vector<Ref<ChunkMesh>>& arr, glm::ivec3 position)
//{
//	for (auto& data : arr) {
//		if (data->GetPosition() == position) {
//			return data;
//		}
//	}
//	return nullptr;
//}
//
//ChunkMeshListComponent& ChunkMeshListComponent::AddMesh(glm::ivec3 position, Ref<ChunkMesh> mesh)
//{
//	bool exist = false;
//
//	for (auto& [pos,va] : data)
//	{
//		if (pos == position) {
//			exist = true;
//			break;
//		}
//	}
//	if (!exist) {
//		size_t indices = mesh->GetIndices().size();
//		size_t size = BlockVertex::GetVertexSize();
//		data.emplace_back(std::make_pair(position, VertexArray::Create((uint32_t)indices)));
//		
//		data.back().second->Bind();
//		data.back().second->AddBuffer(VertexBuffer::Create(data.back().second,
//			BlockVertex::GetLayout(),
//		    (const void*)mesh->GetVertices().data(), 
//		    (uint32_t)mesh->GetVertices().size() * (uint32_t)size));
//
//		data.back().second->AddBuffer(IndexBuffer::Create(data.back().second,
//		    mesh->GetIndices().data(), 
//		    (uint32_t)mesh->GetIndices().size()));
//		data.back().second->Unbind();
//	}
//	return *this;
//}
//
//ChunkMeshListComponent& ChunkMeshListComponent::RemoveMesh(const glm::ivec3& position)
//{
//	for (size_t i = 0; i < data.size(); i++)
//	{
//		if (data[i].first == position) {
//			data[i].second->Delete();
//			data.erase(data.begin() + i);
//		}
//	}
//	return *this;
//}
