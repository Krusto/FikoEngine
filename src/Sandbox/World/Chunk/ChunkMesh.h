﻿#pragma once
#include <vector>
#include <Core/UUID.h>
#include <Renderer/VertexArray.h>
#include <Sandbox/World/Chunk/Block.h>
#include <Sandbox/World/Chunk/Chunk.h>
#include <glm/glm.hpp>

class ChunkMesh : public RefCounted{
public:
	ChunkMesh() = default;
	ChunkMesh(Chunk& data);

	auto& GetIndices() { return m_Indices; };
	const auto& GetIndices() const{ return m_Indices; };

	auto& GetVertices() { return m_Vertices; };
	const auto& GetVertices() const { return m_Vertices; };

	void Generate(Chunk& data);
	void Clear();
private:
	void TryAddFace(Chunk& data, const BlockFace_t& blockFace, glm::ivec3 CheckPosition, glm::ivec3 BlockPosition, Block block);
	void AddFace(const BlockFace_t& face, glm::ivec3 position,Block block);

	std::vector<uint32_t> m_Indices;
	std::vector<BlockVertex> m_Vertices;
	glm::ivec3 m_Position{};
	uint32_t m_MaxIndex{};
};

struct ChunkMeshListComponent
{
public:
	std::vector<std::pair<glm::ivec3,Ref<VertexArray>>> data;

	ChunkMeshListComponent& AddMesh(glm::ivec3 position,Ref<ChunkMesh> mesh);
	ChunkMeshListComponent& RemoveMesh(const glm::ivec3& position);
	ChunkMeshListComponent() = default;
	ChunkMeshListComponent(const ChunkMeshListComponent& other) = default;
	UUID id() { return m_id; }
private:
	UUID m_id;
};