#include <Sandbox/World/WorldConstraints.h>
#include <Sandbox/World/Chunk/Chunk.h>
#include <Sandbox/World/Chunk/Block.h>
#include <Sandbox/World/Chunk/ChunkSection.h>
Chunk::Chunk(glm::ivec3 Position)
	:m_Position(Position){
	for (size_t i = 0; i < SECTION_SIZE; i++)
	{
		for (size_t j = 0; j < SECTION_SIZE; j++)
		{
			for (size_t k = 0; k < SECTION_SIZE; k++)
			{
				m_Data[i][j][k] = Block::Air;
			}
		}
	}
}

void Chunk::SetBlock(glm::ivec3 Position, Block block)
{
	//uint8_t SectionNumber = Position.y / SECTION_SIZE;

	//if (!m_Data.contains(SectionNumber)) {
		//m_Data.emplace(SectionNumber, ChunkSection());
		//SetBlock(Position, block);
		//return;
	//}
	//else {
		//uint8_t BlockPositionY = Position.y - SectionNumber * SECTION_SIZE;
		//m_Data[SectionNumber].SetBlock({Position.x,BlockPositionY ,Position.z},block);
	//}
	m_Data[Position.x][Position.y][Position.z] = block;
	//m_Data.SetBlock(Position,block);
}

Block Chunk::GetBlock(glm::ivec3 Position)
{
	//uint8_t SectionNumber = Position.y / SECTION_SIZE;
	//if (SectionNumber >= 0 && m_Data.contains(SectionNumber)) {
		//uint8_t BlockPositionY = Position.y - SectionNumber * SECTION_SIZE;
		//return m_Data[SectionNumber].GetBlock({ Position.x,BlockPositionY ,Position.z });
	//}
	
	if (Position.x < SECTION_SIZE && Position.x >= 0 &&
		Position.y < SECTION_SIZE && Position.y >= 0 &&
		Position.z < SECTION_SIZE && Position.z >= 0
		) {
		return m_Data[Position.x][Position.y][Position.z];
	}
	return Block::Air;
	//return m_Data.GetBlock(Position);
	//return Block::Air;
}

uint32_t Chunk::GetLayerCount() const
{
	//return m_Data.size();
	return 1;
}

uint32_t Chunk::GetHeight() const
{
	return GetLayerCount() * SECTION_SIZE;
}

bool Chunk::IsGenerated() const
{
	return m_IsGenerated;
}

auto& Chunk::GetData() const
{
	return m_Data;
}

auto& Chunk::GetData()
{
	return m_Data;
}

glm::ivec3 Chunk::GetPosition()
{
	return m_Position;
}

glm::ivec3 Chunk::GetPosition() const
{
	return m_Position;
}

void Chunk::SetIsGenerated(bool flag)
{
	m_IsGenerated = flag;
}
