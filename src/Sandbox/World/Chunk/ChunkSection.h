﻿#pragma once
#include <unordered_map>
#include <Sandbox/World/Chunk/Block.h>
#include <Sandbox/World/WorldConstraints.h>
class ChunkSection {
public:
	ChunkSection();

	void SetBlock(glm::lowp_u8vec3 Position, Block block);
	Block GetBlock(glm::lowp_u8vec3 Position);

private:
	uint32_t FlatCoordinate(glm::lowp_u8vec3 Position);
	uint32_t FlatCoordinate(glm::lowp_u8vec3 Position) const;

	uint64_t m_Seed{};
	glm::ivec3 m_Position{};
	Block m_Blocks[SECTION_SIZE][SECTION_SIZE][SECTION_SIZE];
};