﻿#pragma once
#include <glm/glm.hpp>
#include <Sandbox/World/Vertex.h>
enum class Block : uint8_t {
	Air = 0,
	Dirt = 1,
	Grass = 2,
	Stone = 3,
	Sand = 4,
	Water = 5
};

static std::string_view BlockTypeToString(Block block) {
	switch (block) {
	case Block::Air:
		return "Air";
		break;
	case Block::Dirt:
		return "Dirt";
		break;
	case Block::Grass:
		return "Grass";
		break;
	case Block::Stone:
		return "Stone";
		break;
	case Block::Sand:
		return "Sand";
		break;
	case Block::Water:
		return "Water";
		break;
	}
	return "Unexpexted behaviour!";
}

using BlockFace_t = std::vector<BlockVertex>;

const auto FRONT_FACE = std::vector<BlockVertex>({
	{{0, 0, 1 },  VertexData( 0 , 0 , 0 )},
	{{0, 1, 1 },  VertexData( 1 , 0 , 0 )},
	{{1, 1, 1 },  VertexData( 2 , 0 , 0 )},
	{{1, 0, 1 },  VertexData( 3 , 0 , 0 )} });
const auto BACK_FACE = std::vector<BlockVertex>({
	{{0, 0, 0 },  VertexData{ 0 , 1 , 0}},
	{{0, 1, 0 },  VertexData{ 1 , 1 , 0}},
	{{1, 1, 0 },  VertexData{ 2 , 1 , 0}},
	{{1, 0, 0 },  VertexData{ 3 , 1 , 0}} });
const auto BOTTOM_FACE = std::vector<BlockVertex>({
	{{0, 0, 0 },  VertexData{ 0 , 3 , 0}},
	{{0, 0, 1 },  VertexData{ 1 , 3 , 0}},
	{{1, 0, 1 },  VertexData{ 2 , 3 , 0}},
	{{1, 0, 0 },  VertexData{ 3 , 3 , 0}} });
const auto TOP_FACE = std::vector<BlockVertex>({
	{{0, 1, 0 },  VertexData{0 , 2 , 0}},
	{{0, 1, 1 },  VertexData{1 , 2 , 0}},
	{{1, 1, 1 },  VertexData{2 , 2 , 0}},
	{{1, 1, 0 },  VertexData{3 , 2 , 0}} });
const auto LEFT_FACE = std::vector<BlockVertex>({
	{{0, 0, 0 },  VertexData{0, 4, 0}},
	{{0, 1, 0 },  VertexData{1, 4, 0}},
	{{0, 1, 1 },  VertexData{2, 4, 0}},
	{{0, 0, 1 },  VertexData{3, 4, 0}} });
const auto RIGHT_FACE = std::vector<BlockVertex>({
	{{1, 0, 0 },  VertexData{0, 5, 0}},
	{{1, 1, 0 },  VertexData{1, 5, 0}},
	{{1, 1, 1 },  VertexData{2, 5, 0}},
	{{1, 0, 1 },  VertexData{3, 5, 0}} });