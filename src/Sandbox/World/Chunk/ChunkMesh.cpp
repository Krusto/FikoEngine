﻿#include <Sandbox/World/Chunk/ChunkMesh.h>
#include <Sandbox/World/WorldConstraints.h>
#include <Sandbox/World/Chunk/Chunk.h>
#include <Sandbox/World/Vertex.h>
#include <Renderer/VertexBuffer.h>
#include <Renderer/IndexBuffer.h>

ChunkMesh::ChunkMesh(Chunk& data)
{
	Generate(data);
}

void ChunkMesh::Generate(Chunk& data)
{
if (data.IsGenerated()) {
	this->m_Position.x = data.GetPosition().x;
	this->m_Position.y = 0;
	this->m_Position.z = data.GetPosition().y;

	for (uint32_t y = 0; y < SECTION_SIZE; y++)
	{
		for (uint32_t x = 0; x < SECTION_SIZE; x++)
		{
			for (uint32_t z = 0; z < SECTION_SIZE; z++)
			{
				glm::ivec3 blockPosition = glm::ivec3{ x,y,z };
				Block block = data.GetBlock(blockPosition);

				if (block == Block::Air) {
					continue;
				}

				glm::ivec3 FRONT_BLOCK = glm::ivec3{ 0 , 0 , 1 } + blockPosition;
				glm::ivec3 BACK_BLOCK = glm::ivec3{ 0 , 0 ,-1 } + blockPosition;
				glm::ivec3 TOP_BLOCK = glm::ivec3{ 0 , 1 , 0 } + blockPosition;
				glm::ivec3 BOTTOM_BLOCK = glm::ivec3{ 0 , -1 , 0 } + blockPosition;
				glm::ivec3 LEFT_BLOCK = glm::ivec3{ 1 , 0 , 0 } + blockPosition;
				glm::ivec3 RIGHT_BLOCK = glm::ivec3{ -1 , 0 , 0 } + blockPosition;

				//TryAddFace(data, TOP_FACE, blockPosition, TOP_BLOCK, block);
				//TryAddFace(data, BOTTOM_FACE, blockPosition, BOTTOM_BLOCK, block);
				//TryAddFace(data, RIGHT_FACE, blockPosition, RIGHT_BLOCK, block);
				//TryAddFace(data, LEFT_FACE, blockPosition, LEFT_BLOCK, block);
				//TryAddFace(data, FRONT_FACE, blockPosition, FRONT_BLOCK, block);
				//TryAddFace(data, BACK_FACE, blockPosition, BACK_BLOCK, block);
			
				if (data.GetBlock(TOP_BLOCK) == Block::Air) {
					AddFace(TOP_FACE, blockPosition, block);
				}
				if (data.GetBlock(BOTTOM_BLOCK) == Block::Air) {
					AddFace(BOTTOM_FACE, blockPosition, block);
				}
				if (data.GetBlock(LEFT_BLOCK) == Block::Air) {
					AddFace(RIGHT_FACE, blockPosition, block);
				}
				if (data.GetBlock(RIGHT_BLOCK) == Block::Air) {
					AddFace(LEFT_FACE, blockPosition, block);
				}
				if (data.GetBlock(FRONT_BLOCK) == Block::Air) {
					AddFace(FRONT_FACE, blockPosition, block);
				}
				if (data.GetBlock(BACK_BLOCK) == Block::Air) {
					AddFace(BACK_FACE, blockPosition, block);
				}

			}
		}
	}
	m_Vertices.shrink_to_fit();
	m_Indices.shrink_to_fit();
}
}

void ChunkMesh::Clear()
{
	m_Vertices.clear();
	m_Indices.clear();
	m_Vertices.shrink_to_fit();
	m_Indices.shrink_to_fit();
}

void ChunkMesh::TryAddFace(Chunk& data,const BlockFace_t& blockFace, glm::ivec3 CheckPosition, glm::ivec3 BlockPosition, Block block)
{
	if (data.GetBlock(CheckPosition) == Block::Air) {
		AddFace(blockFace, BlockPosition, block);
	}
}

void ChunkMesh::AddFace(const BlockFace_t& face, glm::ivec3 position, Block block)
{
	float packedPosition = (float)((uint32_t)position.x >> 16 | (uint32_t)position.y >> 8 | ( 0xFF & (uint32_t)position.z));

	m_Vertices.emplace_back(BlockVertex(face[0].Position + (glm::vec3)position, face[0].data + VertexData(0, 0, (uint8_t)block), packedPosition));
	m_Vertices.emplace_back(BlockVertex(face[1].Position + (glm::vec3)position, face[1].data +VertexData(0, 0, (uint8_t)block), packedPosition));
	m_Vertices.emplace_back(BlockVertex(face[2].Position + (glm::vec3)position, face[2].data +VertexData(0, 0, (uint8_t)block), packedPosition));
	m_Vertices.emplace_back(BlockVertex(face[3].Position + (glm::vec3)position, face[3].data+VertexData(0, 0, (uint8_t)block), packedPosition));
		
	m_Indices.insert(m_Indices.end(), { m_MaxIndex,m_MaxIndex + 1,m_MaxIndex + 2,m_MaxIndex + 2,m_MaxIndex + 3,m_MaxIndex });
	m_MaxIndex += 4;
}

ChunkMeshListComponent& ChunkMeshListComponent::AddMesh(glm::ivec3 position,Ref<ChunkMesh> mesh)
{
	bool exist = false;

	for (auto& [pos,va] : data)
	{
		if (pos == position) {
			exist = true;
			break;
		}
	}
	if (!exist) {
		size_t indices = mesh->GetIndices().size();
		size_t size = BlockVertex::GetVertexSize();
		data.emplace_back(std::make_pair(position, VertexArray::Create((uint32_t)indices)));

		data.back().second->Bind();
		data.back().second->AddBuffer(VertexBuffer::Create(data.back().second,
			BlockVertex::GetLayout(),
		    (const void*)mesh->GetVertices().data(),
		    (uint32_t)mesh->GetVertices().size() * (uint32_t)size));

		data.back().second->AddBuffer(IndexBuffer::Create(data.back().second,
		    mesh->GetIndices().data(),
		    (uint32_t)mesh->GetIndices().size()));
		data.back().second->Unbind();
	}
	return *this;
}

ChunkMeshListComponent& ChunkMeshListComponent::RemoveMesh(const glm::ivec3& position)
{
	for (size_t i = 0; i < data.size(); i++)
	{
		if (data[i].first == position) {
			data[i].second->Delete();
			data.erase(data.begin() + i);
		}
	}
	return *this;
}

