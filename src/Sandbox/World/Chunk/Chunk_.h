﻿//#pragma once
//#include <glm/glm.hpp>
//#include <Core/UUID.h>
//#include <Core/Ref.h>
//#include <Renderer/VertexArray.h>
//#include <Sandbox/World/Vertex.h>
//#include <Sandbox/World/Block.h>
//#include <Sandbox/World/Generator/Biome.h>
//
//class ChunkData : public RefCounted {
//public:
//	ChunkData() = default;
//	ChunkData(glm::ivec3 ChunkPosition, uint64_t seed);
//	void Generate(glm::ivec3 ChunkPosition, uint64_t seed);
//
//	auto& GetPosition() { return m_ChunkPosition; }
//	const auto& GetPosition() const { return m_ChunkPosition; }
//	
//	Block GetBlock(glm::ivec3 Position);
//	const Block GetBlock(glm::ivec3 Position) const;
//
//	void SetBlock(glm::ivec3 Position, Block block);
//	auto& GetData() { return m_Blocks; }
//	const auto& GetData() const{ return m_Blocks; }
//
//	void Clear();
//	bool IsGenerated() const{ return m_Generated; }
//private:
//	std::vector<Block> m_Blocks{};
//	glm::ivec3 m_ChunkPosition{};
//	uint64_t m_Seed{};
//	bool m_Generated{};
//};
//class ChunkMesh : public RefCounted {
//public:
//	ChunkMesh() = default;
//	ChunkMesh(Ref<ChunkData> chunkData, void* NeighbourChunks);
//	auto& GetPosition() { return m_ChunkPosition; }
//	const auto& GetPosition() const { return m_ChunkPosition; }
//
//	auto& GetVertices() { return vertices; }
//	const auto& GetVertices() const { return vertices; }
//
//	auto& GetIndices() { return indices; }
//	const auto& GetIndices() const { return indices; }
//
//	void Clear();
//private:
//	void AddBlockFace(BlockFace_t blockFace, const glm::vec3& position, const Block& block);
//	std::vector<BlockVertex> vertices{};
//	std::vector<uint32_t> indices{};
//
//	glm::ivec3 m_ChunkPosition{};
//	void* m_NeighbourChunks{};
//	uint32_t maxIndex{};
//};
//
//Ref<ChunkData> GetData(std::vector<Ref<ChunkData>>& arr, glm::ivec3 position);
//Ref<ChunkMesh> GetMesh(std::vector<Ref<ChunkMesh>>& arr, glm::ivec3 position);
//
//struct ChunkMeshListComponent
//{
//public:
//	std::vector<std::pair<glm::ivec3,Ref<VertexArray>>> data;
//
//	ChunkMeshListComponent& AddMesh(glm::ivec3 position,Ref<ChunkMesh> mesh);
//	ChunkMeshListComponent& RemoveMesh(const glm::ivec3& position);
//	ChunkMeshListComponent() = default;
//	ChunkMeshListComponent(const ChunkMeshListComponent& other) = default;
//	UUID id() { return m_id; }
//private:
//	UUID m_id;
//};