#include <Scene/Scene.h>
#include <Sandbox/Player.h>
#include <Sandbox/World/WorldConstraints.h>
#include <Sandbox/World/Chunk/ChunkController.h>
#include <Sandbox/World/Chunk/Chunk.h>
#include <Sandbox/World/Generator/WorldGen.h>
#include <Sandbox/World/Chunk/ChunkMesh.h>
#include <iostream>
#include <ImGUI/ComponentView.h>
#include <imgui.h>
#include <thread>

ChunkController::ChunkController(Scene* scene, uint64_t seed)
	:m_Scene(scene), m_Seed(seed){
	for (size_t i = 0; i < 16; i++)
	{
		for (size_t j = 0; j < 16; j++)
		{
			GenerateChunk({ i-8,0,j-8 });
		}
	}
}

void ChunkController::Update(Player& player, float dt)
{

	if (m_Player != &player)
		m_Player = &player;

	glm::vec3 playerPosition = m_Player->GetPosition();
	playerPosition -= glm::vec3(0, -1, 0);
	playerPosition = glm::floor(playerPosition);
	glm::vec3 currentChunk = glm::floor(glm::vec3(playerPosition.x / (float)SECTION_SIZE,
		(playerPosition.y) / (float)SECTION_SIZE,
		playerPosition.z / (float)SECTION_SIZE));

	glm::vec3 blockRelativePosition = glm::floor(playerPosition - glm::vec3(
		currentChunk.x * (float)SECTION_SIZE,
		currentChunk.y * (float)SECTION_SIZE,
		currentChunk.z * (float)SECTION_SIZE
	));

	Block currentBlock = GetBlock(currentChunk, blockRelativePosition);

	ImGui::Begin("Collider debug");
	ComponentView::DrawVec3Control("Chunk Position", currentChunk);
	ComponentView::DrawVec3Control("Block Position", blockRelativePosition);
	ComponentView::DrawVec3Control("Velocity", player.velocity);
	ComponentView::DrawTextB(BlockTypeToString(currentBlock));
	if (player.onGround) {
		ComponentView::DrawTextB("1");
	}
	else {
		ComponentView::DrawTextB("0");

	}
	ImGui::End();

	if (currentBlock == Block::Air) {
		//player.velocity.y = -0.02;
		glm::vec3 newPos = glm::floor(blockRelativePosition + player.velocity);
		if (blockRelativePosition != newPos) {
			Block downBlock = GetBlock(currentChunk, newPos);
			if (downBlock == Block::Air) {
				player.Move(player.velocity);
				player.onGround = false;
			}
		}
		else {
			player.Move(player.velocity);
			player.onGround = false;
		}
	}
	else if (currentBlock != Block::Air) {
		player.isAirBorne = false;
		player.onGround = true;
		if (player.jumpTicks != 0) {
			player.isJumping = false;
		std::cout << "not j\n";
		}
	}

	//player.Update(dt);
	
	if (player.GetKeyStates()["Space"] && player.jumpTicks == 0) {
		std::cout << "j\n";
		player.isJumping = true;
	}

	if (player.jumpTicks > 0)
		player.jumpTicks--;

	if (glm::abs(player.velocity.y) < 0.005f) {
		player.velocity.y = 0;
	}
	if (player.isJumping) {
		if (player.onGround && player.jumpTicks == 0)
		{
			//player.velocity.y = 0.42f;
			player.isAirBorne = true;
			player.jumpTicks = 10;
		}
	}
	else {
		player.jumpTicks = 0;
	}
	//player.velocity.y -= 0.08f;
	//player.velocity.y *= 0.98f;
}

void ChunkController::GenerateChunk(glm::ivec3 position)
{
	if (!IsChunkGenerated(position)) {
		m_ChunkData.emplace(position,Ref<Chunk>::Create(position));
		
		WorldGen::Generate(*m_ChunkData[position].Raw(),m_Seed);
		
		m_MeshData.emplace(position,
			Ref<ChunkMesh>::Create(*m_ChunkData[position].Raw()));

		m_Scene->FindEntity("World").GetComponent<ChunkMeshListComponent>()
			.AddMesh(position,
			m_MeshData[position]
			);
	}
}

bool ChunkController::IsChunkGenerated(glm::ivec3 position)
{
	return m_ChunkData.contains(position);
}

Block ChunkController::GetBlock(glm::ivec3 chunk, glm::ivec3 blockPosInChunk)
{
	if(m_ChunkData.contains(chunk))
		return m_ChunkData[chunk]->GetBlock(blockPosInChunk);
	return Block::Air;
}
