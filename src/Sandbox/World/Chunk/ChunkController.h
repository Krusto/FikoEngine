﻿#pragma once
#include <cstdint>
#include <Core/Ref.h>
#include <glm/glm.hpp>
#include <Sandbox/World/Chunk/Chunk.h>
#include <Sandbox/World/Chunk/ChunkMesh.h>
#include <glm/gtx/hash.hpp>
class Player;
class Scene;
class ChunkController {
public:
	ChunkController() = default;
	ChunkController(Scene* scene, uint64_t seed);

	void Update(Player& player,float dt);
	void GenerateChunk(glm::ivec3 position);
	bool IsChunkGenerated(glm::ivec3 position);
	Block GetBlock(glm::ivec3 chunk, glm::ivec3 blockPosInChunk);

private:
	std::unordered_map<glm::ivec3, Ref<Chunk>> m_ChunkData;
	std::unordered_map<glm::ivec3, Ref<ChunkMesh>> m_MeshData;
	
	glm::vec3 m_PlayerOldPosition{};
	Player* m_Player{};
	Scene* m_Scene{};
	uint64_t m_Seed{};
};