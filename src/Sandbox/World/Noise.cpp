﻿#include "Noise.h"
extern "C" {
#include <noise1234.h>
}
float Octave::sample(glm::vec2 i) const {
    float u = 1.0f, v = 0.0f;
    for (size_t j = 0; j < this->n; j++) {
        v += noise3(i.x / u, i.y / u, this->seed + j + (this->o * 32)) * u;
        u *= 2.0f;
    }
    return v;
}

float Combined::sample(glm::vec2 i) const {
    return this->n->sample(glm::vec2(i.x + this->m->sample(i), i.y));
}