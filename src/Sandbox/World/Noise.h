﻿#pragma once
#include <glm/glm.hpp>

struct Noise {
    virtual float sample(glm::vec2 i) const = 0;
};

struct Octave : Noise {
    uint64_t seed;
    size_t n;
    float o;

    Octave(uint64_t seed, size_t n, float o) : seed(seed), n(n), o(o) {}
    float sample(glm::vec2 i) const override;
};

struct Combined : Noise {
    Noise* n, * m;

    Combined(Noise& n, Noise& m) : n(&n), m(&m) {}
    float sample(glm::vec2 i) const override;
};
