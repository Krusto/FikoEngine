#include <iostream>
#include <Core/DeltaTime.h>
#include <Sandbox/World/World.h>
#include <Sandbox/World/Chunk/ChunkController.h>
#include <Sandbox/World/Chunk/ChunkMesh.h>

World::World(Scene* scene, uint64_t seed)
	:m_Seed(seed), m_Scene(scene)
{
	scene->AddEntity("World").AddComponent<ChunkMeshListComponent>();
	m_ChunkController = ChunkController(scene, seed);
	m_PlayerOldPosition = glm::vec3(UINT32_MAX, UINT32_MAX, UINT32_MAX);
}

void World::Update(Player& player, float dt)
{
	m_ChunkController.Update(player, dt);
	m_Ticks++;
}

void World::HandlePlayerInput(Player& player,float dt)
{
	glm::ivec3 moveDirection = {};
	if (player.GetKeyStates()["W"]) {
		moveDirection = glm::ivec3( 0,0,-1 );
	}
	if (player.GetKeyStates()["S"]) {
		moveDirection = glm::ivec3(0, 0, 1);
	}
	if (player.GetKeyStates()["A"]) {
		moveDirection = glm::ivec3(-1, 0, 0);
	}
	if (player.GetKeyStates()["D"]) {
		moveDirection = glm::ivec3(1, 0, 0);
	}
	if (player.GetKeyStates()["Space"] && !player.isJumping) {
		moveDirection = glm::ivec3(0, 1, 0);
	}

	//if (m_ChunkController.IsPlayerColliding(player, { 0,-0.01,0 },Block::Air)) {
		//player.onGround = false;
		//player.velocity = glm::vec3(0, -0.01, 0);
	//}
	//else {
		//player.velocity = glm::vec3(0, 0, 0);
	//}
	//player.Move(player.velocity);
}
