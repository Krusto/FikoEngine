#include <Sandbox/World/WorldConstraints.h>
#include <Sandbox/World/Generator/WorldGen.h>
#include <Sandbox/World/Chunk/Chunk.h>
#include <Sandbox/World/Noise.h>
#include <Sandbox/World/Generator/Biome.h>
#include <Sandbox/World/Chunk/Block.h>
void WorldGen::Generate(Chunk & chunk, uint64_t seed)
{
	for (size_t i = 0; i < SECTION_SIZE; i++)
	{
		for (size_t j = 0; j < SECTION_SIZE; j++)
		{
			//for (size_t k = 0; k < SECTION_SIZE; k++)
			//{
				chunk.SetBlock({ i,10,j }, Block::Grass);
			//}
		}
	}
	//auto n = Octave(seed, 6, 0);
	//Octave os[] = {
	//	Octave(seed, 8, 1),
	//	Octave(seed, 8, 2),
	//	Octave(seed, 8, 3),
	//	Octave(seed, 8, 4),
	//	Octave(seed, 8, 5),
	//	Octave(seed, 8, 6) };

	//Combined cs[] = {
	//	Combined(os[0], os[1]),
	//	Combined(os[2], os[3]),
	//	Combined(os[4], os[5]) };

	//for (uint8_t x = 0; x < SECTION_SIZE; x++) {
	//	for (uint8_t z = 0; z < SECTION_SIZE; z++) {
	//		const auto xz_w = glm::ivec2(x + SECTION_SIZE * chunk.GetPosition().x, 
	//			                         z + SECTION_SIZE * chunk.GetPosition().y);

	//		const float base_scale = 1.3f;
	//		int hr,
	//			hl = (int)((cs[0].sample(glm::vec2(xz_w) * base_scale) / 6.0f) - 4.0f),
	//			hh = (int)((cs[0].sample(glm::vec2(xz_w) * base_scale) / 6.0f) + 6.0f);

	//		 //sample biome noise, extra noise
	//		float t = n.sample(xz_w),
	//			r = n.sample(-xz_w);
	//		hr = t > 0 ? hl : glm::max(hh, hl);

	//		 //offset by water level to determine biome
	//		int h = hr + WATER_LEVEL;

	//		Biome biome;
	//		if (h < WATER_LEVEL) {
	//			biome = Biome::Ocean;
	//		}
	//		else if (t < 0.08f && h < WATER_LEVEL + 2) {
	//			biome = Biome::Beach;
	//		}
	//		else {
	//			biome = Biome::Plains;
	//		}

	//		 //dirtsand depth
	//		int d = (int)(r * 1.4f + 5.0f);

	//		Block top;
	//		switch (biome) {
	//		case Biome::Ocean:
	//			if (r > 0.1f || t > 0.01f) {
	//				top = Block::Sand;
	//			}
	//			else {
	//				top = Block::Dirt;
	//			}
	//			break;
	//		case Biome::Beach:
	//			top = Block::Sand;
	//			break;
	//		default:
	//			top = Block::Grass;
	//			break;
	//		}

	//		//build column
	//		for (int y = 0; y < h; y++) {
	//			Block block;

	//			if (y == (h - 1)) {
	//				block = top;
	//			}
	//			else if (y > (h - d)) {
	//				if (top == Block::Grass) {
	//					block = Block::Dirt;
	//				}
	//				else {
	//					block = top;
	//				}
	//			}
	//			else {
	//				block = Block::Stone;
	//			}

	//			chunk.SetBlock({ x, y, z }, block);
	//		}

	//		for (int y = h; y < WATER_LEVEL; y++) {
	//			chunk.SetBlock({ x,y,z }, Block::Water);
	//		}

	//		/*if (biome == PLAINS && rand.next<float>(0, 1) < 0.001) {
	//			tree(chunk, rand, glm::ivec3(x, h, z));
	//		}*/
	//	}
	//}
	chunk.SetIsGenerated(true);
}