﻿#pragma once
class Chunk;
class WorldGen {
public:
	static void Generate(Chunk& chunk, uint64_t seed);
};