﻿#pragma once
#include <cstdint>
#include <Sandbox/World/WorldConstraints.h>
#include <Sandbox/World/Chunk/ChunkController.h>
#include <Sandbox/Player.h>
#include <Scene/Scene.h>
class World : public RefCounted {
public:
	World(Scene* scene, uint64_t seed = 0);

	void Update(Player& player, float dt);
	auto& GetChunkController() { return m_ChunkController; }

private:

	void HandlePlayerInput(Player& player,float dt);

	ChunkController m_ChunkController;
	uint64_t m_Seed{};
	Scene* m_Scene = nullptr;
	Entity m_WorldEntity{};
	uint32_t m_Ticks{};
	glm::ivec3 m_PlayerInChunkPosition{};
	glm::vec3 m_PlayerOldPosition{};
};