﻿#pragma once
#include <Scene/Scene.h>
#include <Scene/Entity.h>
struct PlayerSettings {
public:
	uint32_t viewDistance = 16;
	float moveSpeed = 0.1f;
};
class Player {
public:
	Player(){}
	Player(Scene* scene);
	static Player Create(Scene* scene);

	void Move(glm::vec3 Position);
	void SetPosition(glm::vec3 Position);
	glm::vec3& GetPosition();
	glm::ivec3 GetPositionI();
	const glm::vec3& GetPosition() const;

	glm::vec3& GetRotation();
	const glm::vec3& GetRotation() const;

	void Rotate(glm::vec3 rot);
	void SetRotation(glm::vec3 rot);

	CameraComponent& GetCamera();

	void ProcessMouseMove(uint32_t width, uint32_t height,float dt);
	void ProcessKeyboardPress(int action,int key,float dt);

	void setRotationLock(bool isLocked);
	void resetMouse();

	auto GetKeyStates() { return keyStates; }

	const PlayerSettings& GetSettings() const{ return settings; }
	PlayerSettings& GetSettings() { return settings; }

	glm::ivec3 GetChunkPosition();

	const uint32_t GetViewDistance() const { return settings.viewDistance; }
	void Update(float dt);
	~Player(){}
	
	uint32_t jumpTicks{};
	bool isJumping{};
	float gravity{ 0.08f };
	float drag{ -0.02f };
	glm::vec3 velocity{};
	bool onGround{};
	bool isAirBorne = true;

private:
	Scene* m_Scene = {};
	Entity m_PlayerEntity = {};
	bool m_FirstMouse = true;
	float yaw{}, pitch{};
	float xpos{}, ypos{}, lastX{}, lastY{};
	bool isRotationLocked{};
	std::map<std::string, bool> keyStates;
	glm::vec3 moveDirection{ 0.0,0.0,0.0 };
	PlayerSettings settings{};
	glm::vec3 m_Position{};
	

};