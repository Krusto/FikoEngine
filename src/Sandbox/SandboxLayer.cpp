﻿#include <iostream>
#include "SandboxLayer.h"
#include <Renderer/Viewport.h>
#include <imgui.h>
#include <Scene/Entity.h>
#include <Renderer/OpenGL/OpenGLShader.h>
#include <Core/Ref.h>
#include <ImGUI/ComponentView.h>
#include <lua.hpp>
#include <Sandbox/World/Chunk/ChunkMesh.h>
SandboxLayer::SandboxLayer()
{
    m_Name = "SandboxLayer";
    m_CurrentScene = new Scene();
    m_WorldShader = Shader::Load("./assets/ChunkShader");
    m_StandardShader = Shader::Load("./assets/DefaultShader");
    m_CurrentScene->AddEntity("Player");
    lua_State* state = luaL_newstate();
    luaL_dostring(state, "x=42");
    lua_close(state);
}
void SandboxLayer::Init(Window* window)
{
    this->window = window;

    m_Framebuffer = Framebuffer::Create(window->width, window->height);
    m_Player = Player::Create(m_CurrentScene);
    m_Player.GetCamera().camera = Ref<Camera>::Create(CameraSpec(m_Framebuffer->GetViewportSize(), 60.0f, 0.3f, 1000.0f));
    m_Player.SetPosition(glm::vec3(0, 40, 0));
    m_Player.SetRotation(glm::vec3(90, 180, 0));
    m_Player.GetCamera().primary = true;

    myWorld = Ref<World>::Create(m_CurrentScene, 0);

    Entity worldEntity = m_CurrentScene->FindEntity("World");

    std::vector<std::string> blockTextureFiles = {
        "dirt.png",
        "grass_block_side.png",
        "grass_block_top.png",
        "stone.png",
        "sand.png",
        "water.png"
    };

    worldEntity.AddComponent<TextureArrayComponent>();
    worldEntity.AddComponent<MaterialComponent>();
    worldEntity.AddComponent<TransformComponent>();

    worldEntity.GetComponent<TextureArrayComponent>().texture = TextureArray::Create("./assets/textures/block", blockTextureFiles);
    worldEntity.GetComponent<TransformComponent>().position = { 0,0,0 };
    worldEntity.GetComponent<MaterialComponent>().material = Material::Create(m_WorldShader, "a");

    cubeMesh = MeshBuilder::CreateMesh(MeshType::Cube);

    Entity playerEntity = m_CurrentScene->FindEntity("Player");
    playerEntity.AddComponent<TransformComponent>();
    playerEntity.AddComponent<MeshComponent>();
    playerEntity.GetComponent<MeshComponent>().mesh = cubeMesh;


    playerEntity.GetComponent<MeshComponent>().va = VertexArray::Create((uint32_t)cubeMesh.indices.size());
    playerEntity.GetComponent<MeshComponent>().va->Bind();
    auto vb = VertexBuffer::Create(playerEntity.GetComponent<MeshComponent>().va, Vertex::GetLayout(), (float*)cubeMesh.vertices.data(), (uint32_t)cubeMesh.vertices.size());
    auto ib = IndexBuffer::Create(playerEntity.GetComponent<MeshComponent>().va, cubeMesh.indices.data(), (uint32_t)cubeMesh.indices.size());
    playerEntity.GetComponent<MeshComponent>().va->Unbind();

    playerEntity.GetComponent<TransformComponent>().position = { 0,0,0 };
   
    m_ChunkTransform.size = { 1,1,1 };
}

void SandboxLayer::OnAttach()
{
}

void SandboxLayer::OnDetach()
{
}

void SandboxLayer::OnUpdate(float dt)
{
    window->Clear();
    m_Player.Update(dt);
    
    myWorld->Update(m_Player, dt);

    Entity selectedEntity;
    for (auto& entity : m_CurrentScene->GetEntitiesWith<CameraComponent>()) {
        if ((Entity{ entity,m_CurrentScene }).GetComponent<CameraComponent>().primary == true) {
            selectedEntity = Entity{ entity,m_CurrentScene };
        }
    }
    if (selectedEntity.IsValid()) {
        auto& entityCameraTransform = m_Player.GetCamera().camera->GetPosition();
        auto& camera = m_Player.GetCamera().camera;
        camera->Update();

        m_Framebuffer->Bind();
        Renderer::ClearColor({ 0.0,0.0,0.0,1.0 });

        Entity worldEntity = m_CurrentScene->FindEntity("World");
        Entity playerEntity = m_CurrentScene->FindEntity("Player");
        if (worldEntity.IsValid()) {
            worldEntity.GetComponent<TextureArrayComponent>().texture->Bind();
            if (worldEntity.HasComponent<MaterialComponent>()) {
                auto& material = worldEntity.GetComponent<MaterialComponent>().material;
                if (material != nullptr)
                    if (material->GetShader() != nullptr) {
                        material->GetShader()->Bind();
                        material->UpdateForRendering();

                        auto shader = material->GetShader();
                        shader->SetUniform("u_ViewPos", camera->GetPosition());
                        shader->SetUniform("u_LightPos", lightPos);
                        selectedEntity.GetComponent<CameraComponent>().camera->Upload(shader);

                        for (auto& [position, va] : worldEntity.GetComponent<ChunkMeshListComponent>().data) {
                            m_ChunkTransform.position = { position.x * SECTION_SIZE,0,position.z * SECTION_SIZE };
                            shader->SetUniform("u_Model", m_ChunkTransform.GetTransform());
                            if (va != nullptr)
                               Renderer::DrawIndexed(va);
                        }
                    }
            }
        }
        if (m_CurrentScene->FindEntity("Player").IsValid()) {
            auto cube = m_CurrentScene->FindEntity("Player").GetComponent<MeshComponent>();
            selectedEntity.GetComponent<CameraComponent>().camera->Upload(m_StandardShader);
            m_StandardShader->SetUniform("u_Model", m_CurrentScene->FindEntity("Player").GetComponent<TransformComponent>().GetTransform());
            Renderer::DrawIndexed(cube.va);
        }

        m_Framebuffer->Unbind();
    }

    deltaTime = timer.Mark();

}
void SandboxLayer::OnWindowResizeEvent(int width, int height) {
}
void SandboxLayer::OnMouseButtonEvent(int button, int action, int mods)
{
    if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_MIDDLE) {
        isScrollPressed = true;
        isRotationEnabled = true;
        isScrollReleased = false;
        m_Player.setRotationLock(false);
    }
    else if (action == GLFW_RELEASE && button == GLFW_MOUSE_BUTTON_MIDDLE) {
        isScrollPressed = false;
        isScrollReleased = true;
        isRotationEnabled = false;
        m_Player.resetMouse();
        m_Player.setRotationLock(false);
    }
}
void SandboxLayer::OnCursorMoveEvent(double xpos, double ypos)
{
    if (isRotationEnabled)
        m_Player.ProcessMouseMove((uint32_t)xpos, (uint32_t)ypos, deltaTime);
}
void SandboxLayer::OnScrollEvent(double xoffset, double yoffset)
{
    m_Player.GetCamera().camera->ChangeFOV((float)-yoffset);
}
void SandboxLayer::OnImGuiDraw()
{

    if (ImGui::BeginMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            if (ImGui::MenuItem("Exit")) { SetShouldExit(true); }
            ImGui::EndMenu();
        }

        ImGui::EndMenuBar();
    }

    ImGui::Begin("Viewport"); {
        ImVec2 size = ImGui::GetContentRegionAvail();
        ViewportSize vSize = ViewportSize{ (uint32_t)size.x, (uint32_t)size.y };
        if (m_ViewportSize != vSize) { // intellisense bug
            m_Framebuffer->Resize((uint32_t)size.x, (uint32_t)size.y);


            for (auto& entity : m_CurrentScene->GetEntitiesWith<CameraComponent>()) {
                if ((Entity{ entity,m_CurrentScene }).GetComponent<CameraComponent>().primary == true) {
                    Entity{ entity,m_CurrentScene }.GetComponent<CameraComponent>().camera->ChangeViewport((uint32_t)size.x, (uint32_t)size.y);
                }
            }

        }
        ImGui::Image((ImTextureID)m_Framebuffer->GetColorAttachmentID(), size);

        ImGui::End();
    }
    ImGui::Begin("Stats"); {
        glm::vec3 a = m_Player.GetPosition();
        ComponentView::DrawVec3Control("Position", a);
        m_Player.SetPosition(a);
        ComponentView::DrawVec3Control("Rotation", m_Player.GetCamera().camera->GetRotation());
        ComponentView::DrawVec3Control("Light Position", lightPos);
        ImGui::Text("FPS: %f", 1.0f/(deltaTime*0.001));
        m_Player.Update(deltaTime);
        ImGui::End();
    }
}

void SandboxLayer::OnMouseMoveEvent(int width, int height)
{
}

void SandboxLayer::OnKeyboardEvent(int action, int key)
{
    if (action == GLFW_PRESS) {
        switch (key) {
        case GLFW_KEY_R:
            m_WorldShader->Reload();
            m_StandardShader->Reload();
            std::cout << "Reloaded!\n";
            break;
        }
    }
    m_Player.ProcessKeyboardPress(action, key, deltaTime);
}

