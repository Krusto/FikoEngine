#include "Player.h"
#include <iostream>
#include <Sandbox/World/WorldConstraints.h>
Player::Player(Scene* scene)
{
	m_Scene = scene;
	m_PlayerEntity = m_Scene->AddEntity("Player")
		.AddComponent<TransformComponent>()
		.AddComponent<CameraComponent>();
    keyStates.emplace(std::make_pair("W", false));
    keyStates.emplace(std::make_pair("S", false));
    keyStates.emplace(std::make_pair("A", false));
    keyStates.emplace(std::make_pair("D", false));
    keyStates.emplace(std::make_pair("Space", false));
}

Player Player::Create(Scene* scene)
{
	return Player(scene);
}

void Player::Move(glm::vec3 Position)
{
    m_Position += Position;
    m_PlayerEntity.GetComponent<CameraComponent>().camera->Move(Position);
}

void Player::SetPosition(glm::vec3 Position)
{
    m_Position = Position;
    m_PlayerEntity.GetComponent<CameraComponent>().camera->SetPosition(Position + glm::vec3{0.5, 1.8, 0.5});
}

glm::vec3& Player::GetPosition()
{
    return m_Position;
}

glm::ivec3 Player::GetPositionI()
{
    return glm::ivec3(
        (int)glm::floor(m_Position.x),
        (int)glm::floor(m_Position.y), 
        (int)glm::floor(m_Position.z));
}

const glm::vec3& Player::GetPosition() const
{
    return m_Position;
}

glm::vec3& Player::GetRotation()
{
    return m_PlayerEntity.GetComponent<CameraComponent>().camera->GetRotation();
}

const glm::vec3& Player::GetRotation() const
{
    return m_PlayerEntity.GetComponent<CameraComponent>().camera->GetRotation();
}

void Player::Rotate(glm::vec3 rot)
{
    m_PlayerEntity.GetComponent<CameraComponent>().camera->Rotate(rot);
}

void Player::SetRotation(glm::vec3 rot)
{
    m_PlayerEntity.GetComponent<CameraComponent>().camera->SetRotation(rot);
}

CameraComponent& Player::GetCamera()
{
	return m_PlayerEntity.GetComponent<CameraComponent>();
}

void Player::ProcessMouseMove(uint32_t width, uint32_t height,float dt)
{
    if (m_FirstMouse)
    {
        lastX = (float)width;
        lastY = (float)height;
        m_FirstMouse = false;
    }

    float xoffset = (float)width - lastX;
    float yoffset = lastY - (float)height;
    lastX = (float)width;
    lastY = (float)height;

    float sensitivity = 0.7f;
    xoffset *= sensitivity*dt;
    yoffset *= sensitivity*dt;


    yaw+= xoffset;
    pitch += yoffset;

    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    GetCamera().camera->Rotate(glm::vec3 {yoffset,-xoffset, 0 });
}

void Player::ProcessKeyboardPress(int action, int key, float dt)
{
    const glm::vec3& rotation = GetCamera().camera->GetRotation();
    if (action == GLFW_PRESS) {
        switch (key) {
            case GLFW_KEY_W:
                keyStates["W"] = true;
              //  moveDirection.x = (float)glm::cos(glm::radians(rotation.y + 90.0));
              //  moveDirection.z = (float)glm::sin(glm::radians(rotation.y + 90.0));
                break;
            case GLFW_KEY_S:
                keyStates["S"] = true;
               // moveDirection.x = (float)-glm::cos(glm::radians(rotation.y + 90.0));
               // moveDirection.z = (float)-glm::sin(glm::radians(rotation.y + 90.0));
                break;
            case GLFW_KEY_A:
                keyStates["A"] = true;
               // moveDirection.x = (float)-glm::cos(glm::radians(rotation.y));
               // moveDirection.z = (float)-glm::sin(glm::radians(rotation.y));
                break;
            case GLFW_KEY_D:
                keyStates["D"] = true;
               // moveDirection.x = (float)glm::cos(glm::radians(rotation.y));
               // moveDirection.z = (float)glm::sin(glm::radians(rotation.y));
                break;
            case GLFW_KEY_SPACE:
                keyStates["Space"] = true;
                isJumping = true;
                // moveDirection.x = (float)glm::cos(glm::radians(rotation.y));
                // moveDirection.z = (float)glm::sin(glm::radians(rotation.y));
                break;

        }
    }
    if (action == GLFW_RELEASE) {
        switch (key) {
        case GLFW_KEY_W:
            keyStates["W"] = false;
            break;
        case GLFW_KEY_S:
            keyStates["S"] = false;
            break;
        case GLFW_KEY_A:
            keyStates["A"] = false;
            break;
        case GLFW_KEY_D:
            keyStates["D"] = false;
            break;
        case GLFW_KEY_SPACE:
            keyStates["Space"] = false;
            isJumping = false;
            break;
        }
        moveDirection = {};
    }
    //Update(dt);
}

void Player::setRotationLock(bool isLocked)
{
    isRotationLocked = isLocked;
}

void Player::resetMouse()
{
    m_FirstMouse = true;
}

glm::ivec3 Player::GetChunkPosition()
{
    auto position = GetPosition();
    throw;
    return {};
}

void Player::Update(float dt)
{
    Move(moveDirection * settings.moveSpeed);
}
