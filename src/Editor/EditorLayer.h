﻿#pragma once
#include <Layer/Layer.h>
#include <Core/Window.h>
#include <Renderer/Renderer.h>
#include <Editor/HierarchyPanel.h>
#include <Editor/InspectorPanel.h>
#include <Scene/Scene.h>
#include <Renderer/Shader.h>
class EditorLayer : public Layer {
public:
    EditorLayer();
    void Init(Window* window) override;
    void OnAttach() override;
    void OnDetach() override;
    void OnUpdate(float dt) override;
    void Destroy() override {};
    void OnImGuiDraw() override;
    void OnMouseMoveEvent(int width, int height) override {};
    void OnKeyboardEvent(int action, int key) override {};
    void OnWindowResizeEvent(int width, int height) override;
    void OnWindowShouldCloseEvent() override { SetShouldExit(true); window->Close(); };
    void OnMouseButtonEvent(int button, int action, int mods) override {};
    void OnCursorMoveEvent(double xpos, double ypos) override {};
    void OnScrollEvent(double xoffset, double yoffset) override {};

protected:
    Window* window = nullptr;
    ViewportSize windowSize;
    
    Scene* m_CurrentScene;
    
    Ref<Framebuffer> m_Framebuffer;
    Ref<Shader> m_Shader;
    Ref<Material> m_ExampleMaterial;

    ViewportSize m_ViewportSize;

    HierarchyPanel m_HierarchyPanel;
    InspectorPanel m_InspectorPanel;

    Entity m_SelectedEntity;
};