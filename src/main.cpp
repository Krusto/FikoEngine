#include <iostream>
#include <Core/Application.h>
using namespace std;
int main(){

	Application FikoEditor("Fiko Editor");
	FikoEditor.Init();

	if (!FikoEditor.Run()) {
		exit(-1);
	}
	return 0;
}