#include <Renderer/RendererApi.h>
#include "TextureArray.h"
#include <Renderer/OpenGL/OpenGLTextureArray.h>
Ref<TextureArray> TextureArray::Create(std::string Path, std::vector<std::string> names)
{
    switch (RendererApi::Current()) {
    case RendererApi::API::OpenGL:
        return Ref<OpenGLTextureArray>::Create(Path, names);
    default:
        exit(-1);
        break;
    }
}