#pragma once
#include <vector>
#include <Core/UUID.h>
#include "Vertex.h"

enum class MeshType {
    Quad,
    Triangle,
    Cube,
    Custom,
    None
};
enum class QuadSide {
    Front,
    Back,
    Left,
    Right,
    Top,
    Bottom,
    None
};
static inline std::string MeshTypeToString(MeshType type) {
    switch (type) {
    case MeshType::Quad:
        return "Quad";
        break;
    case MeshType::Triangle:
        return "Triangle";
        break;
    case MeshType::Custom:
        return "Custom";
        break;
    case MeshType::None:
        return "None";
        break;
    case MeshType::Cube:
        return "Cube";
        break;
    }
    return "Unknown";
}

struct Mesh{
public:
    std::vector<float> vertices{};
    std::vector<uint32_t> indices{};
    uint32_t maxIndex{};
    UUID id;
};
namespace std
{
    template<> struct hash<Mesh>
    {
        auto operator()(Mesh const& m) const noexcept
        {
            return m.id;
        }
    };
}
struct Mesh2D {
public:
    std::vector<Vertex2D> vertices{};
    std::vector<uint32_t> indices{};
    UUID id = 0;
};
namespace std
{
    template<> struct hash<Mesh2D>
    {
        auto operator()(Mesh2D const& m) const noexcept
        {
            return m.id;
        }
    };
}
