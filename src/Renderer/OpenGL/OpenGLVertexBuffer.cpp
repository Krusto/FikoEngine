#include "OpenGLVertexBuffer.h"
#include <glad/glad.h>

#include <utility>
#include <cassert>

OpenGLVertexBuffer::OpenGLVertexBuffer(Ref<VertexArray> va,const VertexLayout& layout,const void* data, uint32_t length) {
    va->Bind();
    glGenBuffers(1, &m_ID);
    glBindBuffer(GL_ARRAY_BUFFER, m_ID);
    glBufferData(GL_ARRAY_BUFFER, length, data, GL_STATIC_DRAW);

    uint32_t index = 0;
    uint32_t offset = 0;
    for (auto attr : layout.attributes) {
        if (attr.second.type != "uint" && attr.second.type != "uvec2" && attr.second.type != "uvec3") {
            glVertexAttribPointer(index, attr.first / sizeof(float), GL_FLOAT, 0, layout.stride, reinterpret_cast<const void*>(offset));
        }
        else {
            glVertexAttribPointer(index, attr.first / sizeof(unsigned int), GL_UNSIGNED_INT, 0, layout.stride, reinterpret_cast<const void*>(offset));
        }
        glEnableVertexAttribArray(index);
        index++;
        offset += attr.first;
    }
}

OpenGLVertexBuffer::OpenGLVertexBuffer(Ref<VertexArray> va, const VertexLayout& layout,const Vertex* data, uint32_t length)
{
    va->Bind();
    glGenBuffers(1, &m_ID);
    glBindBuffer(GL_ARRAY_BUFFER, m_ID);
    glBufferData(GL_ARRAY_BUFFER, length * layout.stride, (void*)data, GL_STATIC_DRAW);

    uint32_t index = 0;
    uint32_t offset = 0;
    for (auto attr : layout.attributes) {
        if (attr.second.type != "int1" && attr.second.type != "uvec2" && attr.second.type != "uvec3") {
            glVertexAttribPointer(index, attr.first / sizeof(float), GL_FLOAT, 0, layout.stride, reinterpret_cast<void*>(offset));
        }
        else {
            glVertexAttribPointer(index, attr.first / sizeof(uint32_t), GL_UNSIGNED_INT, 0, layout.stride, reinterpret_cast<void*>(offset));
        }
        glEnableVertexAttribArray(index);
        index++;
        offset += attr.first;
    }
}

void OpenGLVertexBuffer::Bind() const {
    assert(m_ID != 0);
    glBindBuffer(GL_ARRAY_BUFFER,m_ID);
}

OpenGLVertexBuffer::~OpenGLVertexBuffer()
{
    if (m_ID != 0)
        glDeleteBuffers(1, &m_ID);
}

void OpenGLVertexBuffer::Delete()
{
    glDeleteBuffers(1, &m_ID);
}

OpenGLVertexBuffer OpenGLVertexBuffer::Create(Ref<VertexArray> va,const VertexLayout& layout,const void* data, uint32_t length) {
    return OpenGLVertexBuffer{va,layout,data,length};
}

OpenGLVertexBuffer OpenGLVertexBuffer::Create(Ref<VertexArray> va, const VertexLayout& layout,const Vertex* data, uint32_t length)
{
    return OpenGLVertexBuffer{ va,layout,data,length };
}

