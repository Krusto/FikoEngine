#pragma once
#include <cstdint>
#include <Renderer/IndexBuffer.h>
#include <Renderer/VertexBuffer.h>
#include <Renderer/VertexArray.h>
class OpenGLVertexArray : public VertexArray{
public:
    OpenGLVertexArray()=default;
    explicit OpenGLVertexArray(uint32_t indexCount);
    static OpenGLVertexArray Create(uint32_t indexCount);
    void Bind() const override;
    void Unbind() const override;
    void Delete() override;
    void AddBuffer(Ref<VertexBuffer> vb) override;
    void AddBuffer(Ref<IndexBuffer> ib) override;
private:
    Ref<VertexBuffer> vb;
    Ref<IndexBuffer> ib;
};
