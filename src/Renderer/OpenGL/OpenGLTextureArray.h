﻿#pragma once
#include <Renderer/TextureArray.h>
#include <cstdint>
#include <string_view>
#include <vector>

class OpenGLTextureArray : public TextureArray {
public:
    virtual ~OpenGLTextureArray() override {}
    
    OpenGLTextureArray(std::string Path, std::vector<std::string> names) { this->Load(Path,names); }
    
    virtual void Load(std::string Path, std::vector<std::string> names) override;
    virtual void Bind(uint32_t slot = 0) const override;
    virtual void Destroy() override;

    virtual uint32_t ID() override { return m_id; };
    virtual uint32_t width() override { return m_width; }
    virtual uint32_t height() override { return m_height; }
    virtual uint32_t channels() override { return m_channels; }

protected:
    uint32_t m_id;
    int m_width;
    int m_height;
    int m_channels;
};
