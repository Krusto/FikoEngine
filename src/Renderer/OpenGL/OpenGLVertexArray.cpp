#include <glad/glad.h>
#include <cassert>
#include "OpenGLVertexArray.h"
OpenGLVertexArray::OpenGLVertexArray(uint32_t indexCount){
    glGenVertexArrays(1, &m_ID);
    this->IndexCount = indexCount;
}

void OpenGLVertexArray::Bind() const {
    assert(m_ID != 0);
    glBindVertexArray(m_ID);
}

OpenGLVertexArray OpenGLVertexArray::Create(uint32_t indexCount) {
    return OpenGLVertexArray(indexCount);
}

void OpenGLVertexArray::Unbind() const {
    glBindVertexArray(0);
}

void OpenGLVertexArray::Delete()
{
    if (vb->GetID() != 0)
        vb->Delete();
    if (ib->GetID() != 0)
        ib->Delete();
    if(m_ID !=0)
        glDeleteVertexArrays(1, &m_ID);
}

void OpenGLVertexArray::AddBuffer(Ref<VertexBuffer> vb)
{
    this->vb = vb;
}
void OpenGLVertexArray::AddBuffer(Ref<IndexBuffer> ib)
{
    this->ib = ib;
}
