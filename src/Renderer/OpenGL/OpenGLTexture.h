#pragma once
#include <cstdint>
#include <string_view>
#include <vector>
#include <Renderer/Texture.h>



class OpenGLTexture : public Texture {
public:
    virtual ~OpenGLTexture() override{}
    OpenGLTexture(std::string_view Path) { this->Load(Path); }
    OpenGLTexture(char* data, uint32_t channels, uint32_t width, uint32_t height) { this->Load(data, channels, width, height); }
    virtual void Load(std::string_view Path) override;
    virtual void Load(char* data, uint32_t channels, uint32_t width, uint32_t height) override;
    virtual void Bind(uint32_t slot = 0) const override;
    virtual void Destroy() override;

    virtual uint32_t ID() override{ return m_id; };
    virtual uint32_t width() override{ return m_width; }
    virtual uint32_t height() override { return m_height; }
    virtual uint32_t channels() override { return m_channels; }

    virtual TextureType GetType() override { return TextureType::Texture2D; }
    virtual const TextureType GetType() const override { return TextureType::Texture2D; };
protected:
    uint32_t m_id;
    int m_width;
    int m_height;
    int m_channels;
};

class OpenGLSkyboxTexture {
public:
    OpenGLSkyboxTexture() = default;
    explicit OpenGLSkyboxTexture(std::string_view Path);

    static OpenGLSkyboxTexture Create(std::string_view Path){return OpenGLSkyboxTexture{Path};}

    void Load(std::vector<std::string> Path);

    void Bind() const;

    void Destroy();

    uint32_t id{};
    int width{};
    int height{};
    int channels{};
private:
};