#include "OpenGLTextureArray.h"
#include <filesystem>
#include <iostream>
#include <glad/glad.h>
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_STATIC
#include <Renderer/OpenGL/image/stb_image.h>

void OpenGLTextureArray::Load(std::string Path, std::vector<std::string> names)
{
    for (uint32_t i = 0; i < names.size(); i++)
    {
        if (!std::filesystem::exists(Path+"/"+names[i]))
            std::cout << (std::string(Path) + " does not exist!");
    }

    stbi_set_flip_vertically_on_load(0);
    std::vector<stbi_uc*> data;
    data.reserve(names.size());
    {
        for (uint32_t i = 0; i < names.size(); i++)
        {
            data.emplace_back(stbi_load((Path + "/" + names[i]).c_str(), &m_width, &m_height, &m_channels, 0));
        }
    }
    GLenum internalFormat = 0, dataFormat = 0;
    if (m_channels == 4)
    {
        internalFormat = GL_RGBA;
        dataFormat = GL_RGBA;
    }
    else if (m_channels == 3)
    {
        internalFormat = GL_RGB8;
        dataFormat = GL_RGB;
    }
    else if (m_channels == 1) {
        internalFormat = GL_RED;
        dataFormat = GL_RED;
    }

    glCreateTextures(GL_TEXTURE_2D_ARRAY, 1, &m_id);
    glBindTexture(GL_TEXTURE_2D_ARRAY, m_id);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, internalFormat, m_width, m_height, names.size(), 0,dataFormat, GL_UNSIGNED_BYTE,nullptr);

    uint32_t i = 0;
    for (auto& texture : data) {
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, m_width, m_height, 1, GL_RGBA, GL_UNSIGNED_BYTE, texture);
        i++;
    }

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

for (size_t i = 0; i < data.size();i++) {
        stbi_image_free(data[i]);
    }
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
}

void OpenGLTextureArray::Bind(uint32_t slot) const
{
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D_ARRAY, m_id);
}

void OpenGLTextureArray::Destroy()
{
    glDeleteTextures(1, &m_id);
}
