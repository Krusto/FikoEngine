#pragma once
#include <cstdint>
#include <Core/Ref.h>
#include <Renderer/VertexArray.h>
#include <Renderer/Vertex.h>

class VertexBuffer : public RefCounted {
public:
    static Ref<VertexBuffer> Create(Ref<VertexArray> va, const VertexLayout layout,const Vertex* data, uint32_t length);
    static Ref<VertexBuffer> Create(Ref<VertexArray> va, const VertexLayout layout,const void* data, uint32_t length);

    virtual void Delete() = 0;
    uint32_t GetID() const { return m_ID; }
    virtual void Bind() const = 0;
protected:
    uint32_t m_ID;
};