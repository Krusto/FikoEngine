﻿#pragma once
#include <glm/glm.hpp>
#include <Core/Ref.h>
class VertexArray;
class RendererApi {
public:
    enum class API {
        None,
        OpenGL,
        Vulkan
    };

    virtual void Init() = 0;
    virtual void Shutdown() = 0;
    virtual void BeginFrame() = 0;
    virtual void EndFrame() = 0;
    virtual void RenderQuad() = 0;
    virtual void RenderMesh() = 0;
    virtual void RenderMeshWithMaterial() = 0;
    virtual void ClearColor(glm::vec4 color) = 0;
    virtual void DrawIndexed(Ref<VertexArray>& va) = 0;
    virtual void DrawQuad2D(glm::vec2 position, glm::vec2 size, glm::vec3 color) = 0;
    static API Current() { return RendererApi::SelectedAPI; }
    static void SetApi(API api) { RendererApi::SelectedAPI = api; }

private:
    inline static API SelectedAPI = API::OpenGL;
};