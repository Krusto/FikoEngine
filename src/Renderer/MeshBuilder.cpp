#include "Renderer/MeshBuilder.h"
Mesh MeshBuilder::CreatCube()
{
    Mesh output;
    output.vertices.resize(24);
    output.vertices = std::vector<float>({
            0,0,1,0,0,1,0,0,
            0, 1,1,0,0,1,0,0,
             1, 1,1,0,0,1,0,0,
             1,0,1,0,0,1,0,0,
                               
          0,0,0,0,0,-1,0,0,
          0, 1,0,0,0,-1,0,0,
           1, 1,0,0,0,-1,0,0,
           1,0,0,0,0,-1,0,0,
                               
          0,0,0,0,-1,0,0,0,
          0,0, 1,0,-1,0,0,0,
           1,0, 1,0,-1,0,0,0,
           1,0,0,0,-1,0,0,0,
                               
            0,1,0,0,1,0,0,0,
            0,1, 1,0,1,0,0,0,
             1,1, 1,0,1,0,0,0,
             1,1,0,0,1,0,0,0,

            0,0,0,-1,0,0,0,0,
            0,1, 0,-1,0,0,0,0,
              0,1,1,-1,0,0,0,0,
             0,0,1,-1,0,0,0,0,
                          
              1,0,0,1,0,0,0,0,
              1,1, 0,1,0,0,0,0,
                1,1,1,1,0,0,0,0,
               1,0,1,1,0,0,0,0 });

    output.indices = { 0,1,2,2,3,0,
    4,5,6,6,7,4,
    8,9,10,10,11,8,
    12,13,14,14,15,12,
    16,17,18,18,19,16,
    20,21,22,22,23,20 };

    return output;
}
Mesh MeshBuilder::CreateTriangle()
{
    Mesh output;

    output.vertices = std::vector<float>({
      0,0,0,0,1,0,0,0,
       1, 0,0,0,1,0,0,0,
       1,0,0,0,1,0,0,0
        });
    output.indices = { 2,3,0 };

    return output;
}

Mesh MeshBuilder::CreateQuad(QuadSide side)
{
    Mesh output;
    output.vertices.resize(4);
    switch (side) {
    case QuadSide::Front:
        output.vertices = std::vector<float>({
            0,0,1,0,0,1,0,0,
            0, 1,1,0,0,1,0,0,
             1, 1,1,0,0,1,0,0,
             1,0,1,0,0,1,0,0
            });
        break;
    case QuadSide::Back:
        output.vertices = std::vector<float>({
          0,0,0,0,0,-1,0,0,
          0, 1,0,0,0,-1,0,0,
           1, 1,0,0,0,-1,0,0,
           1,0,0,0,0,-1,0,0
            });
        break;
    case QuadSide::Top:
        output.vertices = std::vector<float>({
          0,0,0,0,-1,0,0,0,
          0,0, 1,0,-1,0,0,0,
           1,0, 1,0,-1,0,0,0,
           1,0,0,0,-1,0,0,0
            });
        break;
    case QuadSide::Bottom:
        output.vertices = std::vector<float>({
            0,1,0,0,1,0,0,0,
            0,1, 1,0,1,0,0,0,
             1,1, 1,0,1,0,0,0,
             1,1,0,0,1,0,0,0
            });
    case QuadSide::Left:
        output.vertices = std::vector<float>({

            0,0,0,-1,0,0,0,0,
            0,1, 0,-1,0,0,0,0,
              0,1,1,-1,0,0,0,0,
             0,0,1,-1,0,0,0,0
            });
        break;
    case QuadSide::Right:
        output.vertices = std::vector<float>({
            0,0,0,-1,0,0,0,0,
            0,1, 0,-1,0,0,0,0,
              0,1,1,-1,0,0,0,0,
             0,0,1,-1,0,0,0,0
            });
        break;
    default:
        output.vertices = std::vector<float>({
              1,0,0,1,0,0,0,0,
              1,1, 0,1,0,0,0,0,
                1,1,1,1,0,0,0,0,
               1,0,1,1,0,0,0,0
            });
        break;

    }
   
    output.indices = { 0,1,2,2,3,0 };

    return output;
}

Mesh MeshBuilder::CreateMesh(MeshType type,QuadSide side)
{
    switch (type) {
    case MeshType::Quad:
        return MeshBuilder::CreateQuad(side);
        break;
    case MeshType::Triangle:
        return MeshBuilder::CreateTriangle();
        break;
    case MeshType::Cube:
        return MeshBuilder::CreatCube();
        break;
    }
    return {};
}
