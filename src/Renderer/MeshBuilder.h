﻿#pragma once
#include <Renderer/Mesh.h>

class MeshBuilder {
public:
    static Mesh CreateMesh(MeshType type,QuadSide side = QuadSide::None);
private:
    static Mesh CreatCube();
    static Mesh CreateTriangle();
    static Mesh CreateQuad(QuadSide side);

};