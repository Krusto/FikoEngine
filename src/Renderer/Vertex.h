#pragma once

#include <glm/gtx/string_cast.hpp>
#include "VertexLayout.h"

struct Vertex{
public:
    glm::vec3 Position{};
    glm::vec3 Normal{};
    glm::vec2 TexCoord{};
    std::string Serialize() const{
        return glm::to_string(this->Position)+","+glm::to_string(this->Normal)+","+glm::to_string(this->TexCoord);
    }
    static VertexLayout GetLayout(){
        return VertexLayout({ {"Position","vec3"},{"Normal","vec3"},{"TexCoord","vec2"} });
    }
};
struct Vertex2D {
public:
    glm::vec3 Position{};
    glm::vec2 TexCoord{};
    glm::vec3 Color{};
    std::string Serialize() const {
        return glm::to_string(this->Position) + "," + glm::to_string(this->TexCoord);
    }
    static VertexLayout GetLayout() {
        return VertexLayout({ {"Position","vec3"},{"TexCoord","vec2"},{"Color","vec3"} });
    }
};