#include "Renderer.h"
#include "RendererApi.h"
#include <Renderer/OpenGL/OpenGLRenderer.h>
#include <Renderer/OpenGL/OpenGLContext.h>
static RendererApi* s_RendererApi = nullptr;

static RendererApi* InitRendererAPI()
{
    switch (RendererApi::Current())
    {
    case RendererApi::API::OpenGL : return new OpenGLRenderer();
    }
    return nullptr;
}

void Renderer::Init()
{
    s_RendererApi = InitRendererAPI();

    s_RendererApi->Init();
}


void Renderer::ClearColor(glm::vec4 color)
{
    s_RendererApi->ClearColor(color);
}

void Renderer::DrawIndexed(Ref<VertexArray> va)
{
    s_RendererApi->DrawIndexed(va);
}

void Renderer::DrawQuad2D(glm::vec2 position, glm::vec2 size, glm::vec4 color)
{

    std::vector<Vertex2D> vertices = {
        {{-0.5,-0.5,0},{0,0},color },
        {{-0.5, 0.5,0},{0,0},color },
        {{ 0.5, 0.5,0},{0,0},color },
        {{ 0.5,-0.5,0},{0,0},color }
    };
    std::vector<uint32_t> indices = { 0,1,2,2,3,0 };
    DrawMesh2D({ vertices,indices });
}

void Renderer::DrawMesh(Mesh& mesh)
{
    auto va = VertexArray::Create((uint32_t)mesh.indices.size());
    va->Bind();
    auto vb = VertexBuffer::Create(va, Vertex::GetLayout(), (float*)mesh.vertices.data(), (uint32_t)mesh.vertices.size());
    auto ib = IndexBuffer::Create(va, mesh.indices.data(), (uint32_t)mesh.indices.size());
    Renderer::DrawIndexed(va);

}
void Renderer::DrawMesh2D(Mesh2D mesh)
{
    auto va = VertexArray::Create((uint32_t)mesh.indices.size());
    va->Bind();
    auto vb = VertexBuffer::Create(va, Vertex2D::GetLayout(), (float*)mesh.vertices.data(), (uint32_t)mesh.vertices.size());
    auto ib = IndexBuffer::Create(va, mesh.indices.data(), (uint32_t)mesh.indices.size());
    Renderer::DrawIndexed(va);
}
void Renderer::DrawMaterialMesh(Mesh mesh, Ref<Material> material)
{
    material->GetShader()->Bind();
    material->UpdateForRendering();
    Renderer::DrawMesh(mesh);
}
Ref<GraphicsContext> Renderer::CreateGraphicsContext(GLFWwindow* handle)
{
    return GraphicsContext::Create(handle);
}
