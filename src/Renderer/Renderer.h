﻿#pragma once
#include <functional>
#include <Core/Ref.h>
#include <Renderer/GraphicsContext.h>
#include <Renderer/Camera.h>
#include <Renderer/Framebuffer.h>
#include <Renderer/IndexBuffer.h>
#include <Renderer/Material.h>
#include <Renderer/MeshBuilder.h>
#include <Renderer/Shader.h>
#include <Renderer/Texture.h>
#include <Renderer/TextureArray.h>
#include <Renderer/TextureAtlas.h>
#include <Renderer/Vertex.h>
#include <Renderer/VertexArray.h>
#include <Renderer/VertexBuffer.h>
#include <Renderer/VertexLayout.h>
#include <Renderer/Light.h>
#include <Renderer/Viewport.h>
struct GLFWwindow;
class Renderer {
public:
    using FunctionContainer_t = std::vector<std::function<void(void)>>;

    static void Init();
    static void Shutdown() {}
    static void BeginFrame() {};
    static void EndFrame() {};
    static void ClearColor(glm::vec4 color);
    static void DrawIndexed(Ref<VertexArray> va);
    static void DrawQuad2D(glm::vec2 position, glm::vec2 size,glm::vec4 color);
    static void DrawMesh(Mesh& mesh);
    static void DrawMesh2D(Mesh2D mesh);
    static void DrawMaterialMesh(Mesh mesh, Ref<Material> material);
    static Ref<GraphicsContext> CreateGraphicsContext(GLFWwindow* handle);
    
    static void Submit(const std::function<void(void)>& func) {
        s_FunctionContainer.push_back(func);
    }
    template<typename T>
    static void SubmitFlush(T func) {
    }
    static void Flush() {
       for(auto& function : s_FunctionContainer){
           function();
       }
       s_FunctionContainer.clear();
    }
    static inline FunctionContainer_t s_FunctionContainer{};
    //static std::uint32_t s_FunctionIndex;
};