﻿#include "Shader.h"
#include <Renderer/RendererApi.h>
#include <Renderer/OpenGL/OpenGLShader.h>
Ref<Shader> Shader::Load(const std::string& path) {
    switch (RendererApi::Current()) {
    case RendererApi::API::OpenGL:
        return Ref<OpenGLShader>::Create(path,false);
        break;
    default:
        exit(-1);
        break;
    }
}
