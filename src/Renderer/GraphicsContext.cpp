#include "GraphicsContext.h"
#include <Renderer/RendererApi.h>
#include <Renderer/OpenGL/OpenGLContext.h>

Ref<GraphicsContext> GraphicsContext::Create(GLFWwindow* window)
{
    switch (RendererApi::Current()) {
    case RendererApi::API::OpenGL:
        return Ref<OpenGLContext>::Create(window);
        break;
    }
    return nullptr;
}
