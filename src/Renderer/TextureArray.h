﻿#pragma once
#include <string>
#include <Core/Ref.h>
#include <vector>
#include <string>
class TextureArray : public RefCounted {
public:
    static Ref<TextureArray> Create(std::string Path, std::vector<std::string> names);

    virtual ~TextureArray() {};
    virtual void Load(std::string Path,std::vector<std::string> names) = 0;
    virtual void Bind(uint32_t slot = 0) const = 0;

    virtual void Destroy() = 0;
    virtual uint32_t ID() = 0;
    virtual uint32_t width() = 0;
    virtual uint32_t height() = 0;
    virtual uint32_t channels() = 0;
};