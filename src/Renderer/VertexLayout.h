﻿#pragma once
#include <string>
#include <vector>
struct VertexAttribute {
public:
    const std::string name;
    const std::string type;
};

struct VertexLayout {
public:
    VertexLayout() = default;
    explicit VertexLayout(const std::vector<VertexAttribute> attributes)
    {
        for (auto& attr : attributes) {
            if (attr.type == "vec2") {
                stride += (uint16_t)sizeof(float) * 2;
                this->attributes.emplace_back(std::make_pair((uint16_t)sizeof(float) * 2, attr));
            }
            else if (attr.type == "vec3") {
                stride += (uint16_t)sizeof(float) * 3;
                this->attributes.emplace_back(std::make_pair((uint16_t)sizeof(float) * 3, attr));
            }
            else if (attr.type == "float") {
                stride += (uint16_t)sizeof(float);
                this->attributes.emplace_back(std::make_pair((uint16_t)sizeof(float), attr));
            }
            else if (attr.type == "uint") {
                stride += (uint16_t)sizeof(unsigned int);
                this->attributes.emplace_back(std::make_pair((uint16_t)sizeof(unsigned int), attr));
            }
            else if (attr.type == "uvec2") {
                stride += (uint16_t)sizeof(unsigned int)*2;
                this->attributes.emplace_back(std::make_pair((uint16_t)sizeof(unsigned int)*2, attr));
            }
            else if (attr.type == "uvec3") {
                stride += (uint16_t)sizeof(uint32_t)*3;
                this->attributes.emplace_back(std::make_pair((uint16_t)sizeof(uint32_t)*3, attr));
            }
        }
    }
    std::vector<std::pair<uint32_t, VertexAttribute>> attributes;
    uint32_t stride{};
};