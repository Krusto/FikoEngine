﻿#include "Framebuffer.h"
#include <Renderer/RendererApi.h>
#include <Renderer/OpenGL/OpenGLFramebuffer.h>
#include <Core/Ref.h>
Ref<Framebuffer> Framebuffer::Create(uint32_t width, uint32_t height){
    switch (RendererApi::Current()) {
    case RendererApi::API::OpenGL:
        return Ref<OpenGLFramebuffer>::Create(width, height);
    }
    return nullptr;
}