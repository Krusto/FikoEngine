#include "LuaState.h"
#include <iostream>
#include <cassert>
LuaState::LuaState(const std::string& filePath)
{
	state = luaL_newstate();
	luaL_openlibs(state);
	if (luaL_dofile(state, filePath.c_str()) != LUA_OK) {
		std::string errormsg = lua_tostring(state, -1);
		std::cout << errormsg << std::endl;
		assert(0);
	}
}

LuaState::~LuaState()
{
	if(state != nullptr)
		lua_close(state);
}
