﻿#pragma once
#include <lua.hpp>
#include <unordered_map>

class LuaState;
using LuaStateList = std::unordered_map<std::string, LuaState>;

class LuaState {
public:
	LuaState() = default;
	LuaState(const std::string& filePath);

	operator lua_State* () { return state; }

	~LuaState();
private:
	lua_State* state;
};